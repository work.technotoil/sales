import React, { useState } from "react"
import dummy from "../../image/product_dummy.png"

const PlaceHolderImage = ({src}) => {const [isLoading, setIsLoading] = useState(true);

  function onLoad() {
    setTimeout(() => setIsLoading(false), 100);
  }

  return (
    <div style={{display:"flex", justifyContent:"center"}}>
      <img alt="" src={dummy} style={{ display: isLoading ? "block" : "none" }} />
      <img alt="" src={src} style={{ display: isLoading ? "none" : "block" }} onLoad={onLoad} />
    </div>
  )
}

export default PlaceHolderImage