import Pusher from "pusher-js";
import { useNavigate } from "react-router-dom";
import { right_login } from "../API/urlconstantsales";


export const PusherJS = () => {
    const navigate = useNavigate()
    var pusher = new Pusher("d7d058d7ebfda821aed0", {
        cluster: "ap2",
    });
    var channel = pusher.subscribe("my-channel");
    var saleaman_id = localStorage.getItem("salesman_id");
    channel.bind(`salesman_${saleaman_id}`, function (data) {
        localStorage.clear();
        navigate(right_login)
    });
}