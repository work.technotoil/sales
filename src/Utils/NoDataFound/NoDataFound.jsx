import React from 'react'
import NoData from "../../image/NoData.jpg"

const NoDataFound = () => {
  return (
    <div className='Nodatafound' style={{textAlign:"center"}}>
      <img src={NoData} alt="" />
    </div>
  )
}

export default NoDataFound
