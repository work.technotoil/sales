import React from 'react'
import "./Index.css"
import Customer from "../../image/Name.svg"
import Home from "../../image/Address.svg"
import Callp from "../../image/callP.svg"
import Image from '../HelperComponent/Image/Index'
import Back from "../../image/BACK.svg"
import { useNavigate } from 'react-router-dom'
import ToolTip from '../../Helper/Tootip'

const UserProfile = ({ UserDetail }) => {
  const navigate = useNavigate()
  return (

    <div className='UserProMain'>
      <div className="HeadBack">
        <Image SRC={Back} onClick={() => { navigate(-1) }} />
        <p className="pageHead"><ToolTip TitleText={UserDetail.customer_name} ParaText={UserDetail.customer_name} /></p>
      </div>
      <div className='UserProfile'>
        <div className="UserData">
          {UserDetail.agent_name ? <div className="UserName">
            <Image SRC={Customer} />
            <p className='OneLineText'><ToolTip TitleText={UserDetail.agent_name} ParaText={UserDetail.agent_name} /></p>       
          </div> : ""}
          {UserDetail.address &&
            <div className="UserName">
              <Image SRC={Home} alt="" />
              <p className='twoLineText'><ToolTip TitleText={UserDetail.address} ParaText={UserDetail.address} /></p>
            </div>}
            {UserDetail.mobile_no ?
          <div className="UserName">
            <Image SRC={Callp} alt="" />            
            <p>{ UserDetail.dial_code ? `${UserDetail.dial_code} ${UserDetail.mobile_no}` : UserDetail.mobile_no}</p>
          </div> : ""}
        </div>
        <div className="UserPoint">
          
          {UserDetail.loyalty_points == 0.000 ? "" : <div className="UserLoyality">
            <p className='UserLabel'>Loyalty Points:</p>
            <p className='userValue'>{UserDetail.loyalty_points}</p>
          </div>}

           
          {UserDetail.credit_limit &&
            <div className="UserLoyality">
              <p className='UserLabel'>Credit Limit:</p>
              <p className='userValue'><span className='Remaining'>100.000.000</span> / <span> 200.000.000 đ</span></p>
            </div>}
          {UserDetail.credit_term &&
            <div className="UserLoyality">
              <p className='UserLabel'>Credit Term:</p>
              <p className='userValue'>{UserDetail.credit_term}</p>
            </div>}
        </div>
      </div>
    </div>
  )



}

export default UserProfile
