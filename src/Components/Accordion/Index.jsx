import { Collapse } from 'antd';
import React, { useEffect, useState } from 'react';
import "./Index.css"
import { UpOutlined } from '@ant-design/icons';
import ToolTip from '../../Helper/Tootip';
import { checkAvailable } from '../../Utils/helper';
const { Panel } = Collapse;
export const Head = ({ item, index }) => {
  return (

    <tr className='accordionHead' key={index}>
      <td className='NumberSales'>{index + 1}</td>
      <td className='CustomerCode CustCode'>{checkAvailable(item.customer_code)}</td>
      <td className='CustomerName CName OnetdneText '><ToolTip TitleText={item.customer_name} ParaText={item.customer_name} /></td>
      <td className=' CustomerAddr cAddress2  callcardaddress'><ToolTip TitleText={item.address} ParaText={item.address} /></td>
      <td className='CustomerPhone cPhone'>{item.dial_code ? `${item.dial_code} ${item.mobile_no}` : `${item.dial_code}${item.mobile_no}`}</td>
    </tr>
  )
}



export const PanelText = ({ val }) => {

  const [filterArr, setFilterArr] = useState([])

  useEffect(() => {
    const checkData = val.filter((item) => {
      return item.first_month || item.second_month
    })
    setFilterArr(checkData)
  }, [])
  const NoProduct = <div className="Nofilter">
    Don't have any Order of this group
  </div>
  const { group_product_name, previous_first_month, previous_second_month } = val
  return (
    filterArr.length ?
      filterArr.map((product) => {
        return (
          <main className="PanelBody">
            <main className="panelText">
              <main className="PanelProduct">
                <p>{product.group_product_name}</p>
              </main>
              <main className="PanelMonth">
                <ul>
                  {product.first_month ? <li>{product.previous_first_month}</li> : ""}
                  {product.second_month ? <li>{product.previous_second_month}</li>: ""}
                </ul>

              </main>
              <main className="PanelPrice">
                <ul>
                {product.first_month ?   <li>{product.first_month}</li> : ""}
                  {product.second_month?<li>{product.second_month}</li> : ""}
                </ul>
              </main>
            </main>
          </main>
        )
      }) : NoProduct

  )
}


const Accordion = ({ apiRes, allProductId }) => {
  const onChange = (key, item) => {
    console.log(key);
  };

  const NoFilter_Text = <div className="Nofilter">
    Please select Product group filter first
  </div>
  const NoProduct = <div className="Nofilter">
    Don't have any Order of this group
  </div>
  return (
    <Collapse accordion ghost onChange={onChange}
      expandIconPosition={"end"}
      expandIcon={({ isActive }) => <UpOutlined rotate={isActive ? 0 : 180} />}
      className={(isActive) => (isActive ? "Hello" : "byee")}
    >
      {apiRes.map((item, index) => {
        return (
          <Panel key={item.user_id} className={(isActive) => (isActive ? "Hello" : "byee")} header={<Head item={item} index={index} />} >
            {/* {allProductId.length ? <p>{<PanelText val={item.order_detail} />}</p> : "please   " */}
            {allProductId.length ? item.order_detail?.length ? <p>{<PanelText val={item.order_detail} />}</p> : NoProduct : NoFilter_Text}
          </Panel>
        )
      })}

    </Collapse>
  );
};

export default Accordion;