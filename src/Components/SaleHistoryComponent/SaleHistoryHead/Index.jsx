import React from "react"
import { useTranslation } from "react-i18next"
import "./Index.css"
const SaleHistoryHead = ({totalSales}) => {
 const {previous_month_first , previous_month_second , previous_month_third} = totalSales
  const { t } = useTranslation()
  return (
    <thead>
      <tr className='SalesHistoryThead'>
        <th>SKUs</th>
        <th></th>
        <th>{previous_month_third}</th>
        <th>{previous_month_second}</th>
        <th>{previous_month_first}</th>
        <th className='AVG_Month'>{t("AVG/Month")}</th>
        <th>Current</th>
        <th className='Suggest'>Suggest Order</th>
      </tr>
    </thead>
  )
}

export default SaleHistoryHead
