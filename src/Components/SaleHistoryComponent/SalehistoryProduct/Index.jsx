import React from "react"
import { BaseUrlImg } from "../../../API/BaseUrl"
import ToolTip from "../../../Helper/Tootip"
import PlaceHolderImage from "../../../Utils/PlaceHolderImage/PlaceHolderImage"
import SaleHistoryHead from "../SaleHistoryHead/Index"
import "./Index.css"
const SalesHistoryProduct = ({ SalesData , totalSales}) => {
  const {first_month_total ,suggest_order_total, second_month_total , third_month_total , average_month_total , current_month_total} = totalSales
  return (

    <div className='SalesHistoryProduct'>   
      <table>
        <SaleHistoryHead totalSales={totalSales}/>        
        <tbody className='SalesHistoryTbody'>
          {SalesData?.map((item) => {
            const{product_name ,suggest_order, product_image , first_month , second_month , third_month , three_months_average, current_month} = item || {}
            return (
              <tr className='SaleHistoryTbody'>
                <td className="ProdName"> <u><ToolTip TitleText={product_name} ParaText={product_name} /></u></td>
                <td className="ProdImg"><PlaceHolderImage src={`${BaseUrlImg}${product_image}`} alt="" /></td>
                <td>{third_month}</td>
                <td>{second_month}</td>
                <td>{first_month}</td>
                <td className='AVG_Month'>{three_months_average.toFixed(2)}</td>
                <td className= {three_months_average > current_month ?'Current Small' : "Current greater"} >{current_month}</td>
                <td>{suggest_order>0 ? suggest_order.toFixed(2) : "-"}</td>
              </tr>
            )
          })}

        </tbody>

        <tfoot>
          <tr className='SalesHistoryFooter'>
            <td>Grand Total</td>
            <td></td>
            <td>{third_month_total}</td>
            <td>{second_month_total}</td>
            <td>{first_month_total}</td>
            <td>{average_month_total && average_month_total.toFixed(2)}</td>
            <td>{current_month_total}</td>
            <td>{ suggest_order_total && suggest_order_total.toFixed(2)}</td>            
          </tr>
        </tfoot>
      </table>        
    </div>        
  )
}

export default SalesHistoryProduct




















// import React from "react"
// import { useTranslation } from "react-i18next"
// import { SpinnerDotted } from "spinners-react"
// import { BaseUrlImg } from "../../../API/BaseUrl"
// import ToolTip from "../../../Helper/Tootip"
// import PlaceHolderImage from "../../../Utils/PlaceHolderImage/PlaceHolderImage"
// import SaleHistoryHead from "../SaleHistoryHead/Index"
// import "./Index.css"
// // import Loader from "../../src/Helper/Loader.jsx";

// const SalesHistoryProduct = ({ SalesData , totalSales,loading}) => {
//   const {first_month_total ,suggest_order_total, second_month_total , third_month_total , average_month_total , current_month_total} = totalSales
//   // const [loading , setLoading]= useState(false)
//   return (

   
//     <div className='SalesHistoryProduct'>
 
//       <table>
//         <SaleHistoryHead totalSales={totalSales}/>
        
//         <tbody className='SalesHistoryTbody'>
//           {SalesData.map((item) => {
//             const{product_name ,suggest_order, product_image , first_month , second_month , third_month , three_months_average, current_month} = item
//             return (
//               <tr className='SaleHistoryTbody'>
//                 <td className="ProdName"> <u><ToolTip TitleText={product_name} ParaText={product_name} /></u></td>
//                 <td className="ProdImg"><PlaceHolderImage src={`${BaseUrlImg}${product_image}`} alt="" /></td>
//                 <td>{third_month}</td>
//                 <td>{second_month}</td>
//                 <td>{first_month}</td>
//                 <td className='AVG_Month'>{three_months_average.toFixed(2)}</td>
//                 <td className= {three_months_average > current_month ?'Current Small' : "Current greater"} >{current_month}</td>
//                 {/* dharmesh */}
//                 {/* <td className='AVG_Month'>{Number(three_months_average).toFixed(2) ? Number(three_months_average).toFixed(2) : null}</td>
//                 <td className= {three_months_average > current_month ?'Current Small' : "Current greater"} >{current_month}</td> */}
//                 <td>{suggest_order>0 ? suggest_order.toFixed(2) : "-"}</td>
//                 {/* dharmesh */}
//               </tr>
//             )
//           })}

//         </tbody>

//         <tfoot>
//           <tr className='SalesHistoryFooter'>
//             <td>Grand Total</td>
//             <td></td>
//             <td>{third_month_total}</td>
//             <td>{second_month_total}</td>
//             <td>{first_month_total}</td>
//             <td>{average_month_total.toFixed(2)}</td>
//             <td>{current_month_total}</td>
//             <td>{suggest_order_total.toFixed(2)}</td>
//           </tr>
//         </tfoot>

//       </table>

        
//     </div>

        

//   )
// }

// export default SalesHistoryProduct


// // "product_id": 2,
// // "product_name": "BÃ¡nh Que Báº¯p HÆ°Æ¡ng BÆ¡ Stick Biscuit 20g",
// // "third_month": 0,
// // "second_month": 0,
// // "first_month": 2,
// // "current_month": 0,
// // "three_months_average": 0.6666666666666666,
// // "product_image": "/Product_images/product2.png"