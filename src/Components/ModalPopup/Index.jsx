

import React, { useState } from 'react';
import { Button, Modal } from 'antd';
const ModalPopup = ({BtnText, children , isModalOpen,showModal,handleOk,handleCancel }) => {
    return (
        <>
            <p type="primary" onClick={showModal}>
               {BtnText}
            </p>
            <Modal className='ProductModal' footer={null} visible={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <h5>Select Product Group(s) to display:</h5>
                {children}
            </Modal>
        </>
    );
};
export default ModalPopup;
