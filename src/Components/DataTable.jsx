import React, { useEffect, useState } from 'react';
import DataTable from 'react-data-table-component';
import {  AllProduct1,  BaseUrl1 } from '../API/BaseUrl.jsx';
import { CSVLink, CSVDownload } from "react-csv"
import Export from "react-data-table-component"
import './DataTable.css'
import SalesLayout from '../Layout/SalesLayout/SalesLayout.jsx';
const sortIcon = "";
const selectProps = { indeterminate: isIndeterminate => isIndeterminate };

function DataTableBase(props) {

    const [DataItem, setDataItem] = useState([])
    const [loader, setLoader] = useState(true)

    const columns = [
        {
            name: 'Id',
            selector: row => row.product_id,
            sortable: true,
            main :{
                first : "first",
                second : "second"
            }
        },
        {
            name: 'Product Name',
            selector: row => row.product_name,
            sortable: true,
        },
        {
            name: 'Product Discription',
            selector: row => row.product_desc,
            sortable: true,
        },

        {
            name: 'Stock',
            selector: row => row.year,
            sortable: true,
            
            
            
        },

        {
            name: 'Action',
            selector: row => row.year,
            sortable: true,
            cell: row => <button onClick={()=>btnClick(row)}>View</button>
        },
    ];
    const btnClick=(row)=>{
   
    }

    return (
        
            <SalesLayout>
                {loader ? "Loading..." :  <DataTable
                    pagination
                    selectableRowsComponentProps={selectProps}
                    sortIcon={sortIcon}
                    dense
                    columns={columns}
                    data={DataItem}
                    subHeader={true}
                  
                    {...props}

                    highlightOnHover
                    title="Product List"
                    fixedHeader

                />}
            </SalesLayout>

    )
}

export default DataTableBase;