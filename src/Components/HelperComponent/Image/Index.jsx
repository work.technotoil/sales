import React from 'react'

const Image = ({SRC , onClick}) => {
  return (
    <img src={SRC} onClick={onClick} alt="" />
  )
}

export default Image
