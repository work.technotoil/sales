/* eslint-disable linebreak-style */
import { configureStore } from "@reduxjs/toolkit";
import { persistStore, persistReducer } from "redux-persist" // imports from redux-persist
import storage from "redux-persist/lib/storage" // defaults to localStorage for web

import reducer from "./reducer"; 

const persistConfig = { // configuration object for redux-persist
  key: "root",
  storage, // define which storage to use
}

const persistedReducer = persistReducer(persistConfig, reducer) // create a persisted reducer

export const store = configureStore({ // store configuration
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
      immutableCheck: false,
    }),
});

export const  persistor = persistStore(store); // used to create the persisted store, persistor will be used in the next step