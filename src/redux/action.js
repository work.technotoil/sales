/* eslint-disable linebreak-style */
const Action = {
  AREA_ID: "AREA_ID",
  AREA_NAME: "AREA_NAME",
  ROUTE_NO : "ROUTE_NO",
  USER_PROFILE : "USER_PROFILE"

}
export const setAreaId = (AreaId) => {
  return {
    type: Action.AREA_ID,
    AreaId
  }
}
export const setAreaName = (AreaName) => {
  return {
    type: Action.AREA_NAME,
    AreaName
  }
}
export const setRouteNo = (RouteNo) => {
  return {
    type: Action.ROUTE_NO,
    RouteNo
  }
}
export const setUserProfile = (UserProfile) => {
  return {
    type: Action.USER_PROFILE,
    UserProfile
  }
}


export default Action

