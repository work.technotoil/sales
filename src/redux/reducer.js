/* eslint-disable linebreak-style */
import Action from "./action";

const initialState = {
  AreaId: [],
  AreaName : [],
  RouteNo : [],
  UserProfile : []
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Action.AREA_ID:
      return {
        ...state,
        AreaId: action.AreaId
      }
    case Action.AREA_NAME:
      return {
        ...state,
        AreaName: action.AreaName
      }
    case Action.ROUTE_NO:
      return {
        ...state,
        RouteNo: action.RouteNo
      }
    case Action.USER_PROFILE:
      return{
        ...state,
        UserProfile : action.UserProfile
      }
    default:
      return state
  }


}

export default reducer