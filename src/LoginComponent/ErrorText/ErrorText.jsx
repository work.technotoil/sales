import React from 'react';
import './ErrorText.css'


const ErrorText = ({error}) => {

    return (
    <p className='errorText'>{error}</p>
    
    )
}

export default ErrorText;