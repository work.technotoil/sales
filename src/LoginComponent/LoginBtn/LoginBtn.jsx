import React from 'react';
import { useTranslation } from 'react-i18next';
import './LoginBtn.css'


const LoginBtn = ({BtnText,loading, onClick=null , ImgSrc ,BtnSpan,disable=false }) => {

const { t } = useTranslation();

  return (
    <div className='button_container' >
       <button disabled={loading || disable} className={disable ? "shopbtn DisableBtn" : "shopbtn"} onClick={onClick}> {loading ? t('Loading...') : BtnText} <span>{BtnSpan}</span> <img src={ImgSrc} alt="" /> </button>
    </div>
  )
}

export default LoginBtn;