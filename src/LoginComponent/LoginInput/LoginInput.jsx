import React from 'react';
import './LoginInput.css'

const LoginInput = ({Label, Placeholder, type, value, callBack, fieldType, error }) => {

    return (

        <div className={error ? "error_password" : "inputArea"}>
            <div> <label className='required'>{Label}</label></div>
            <input type={type} placeholder={Placeholder} label={Label} value={value} onChange={(e) => {
                callBack(e.target.value, fieldType)
            }} />
        </div>
    )
}

export default LoginInput;