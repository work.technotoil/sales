/* eslint-disable linebreak-style */
import React, { useEffect } from "react"
import "./Index.css"
import SalesHistoryProduct from "../../Components/SaleHistoryComponent/SalehistoryProduct/Index"
import SalesLayout from "../../Layout/SalesLayout/SalesLayout"
import UserProfile from "../../Components/UserProfile/Index"
import { useLocation } from "react-router-dom"
import { BaseUrl, customer_detail, sales_history } from "../../API/BaseUrl"
import { useState } from "react"
import { SpinnerDotted } from 'spinners-react';
import NoDataFound from "../../Utils/NoDataFound/NoDataFound"
import PaginationTable from "../CustomerManagement/PaginationTable/PaginationTable"
import { useDispatch } from "react-redux"
import { setUserProfile } from "../../redux/action"
import Loader from "../../Helper/Loader"

let limit = 10
const SaleHistory = () => {
  const location = useLocation()
  const [UserDetail, setUserDetail] = useState([])
  const [loading, setLoading] = useState(false)
  const [SalesData, setSalesData] = useState([])
  const [totalSales, setTotalSales] = useState("")
  const [pageCount, setPageCount] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)
  const Main = location.state.UserId


  const dispatch = useDispatch()
  const ApiExtract = async () => {
    try {
      setLoading(true)
      const { data } = await BaseUrl.post(customer_detail, {
        user_id: Main
      })
      if (data.status) {
        setUserDetail(data.customer_data)
        dispatch(setUserProfile(data.customer_data))
      }
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }



  const SaleHistoryExtract = async () => {
    try {
      setLoading(true)
      const { data } = await BaseUrl.post(sales_history, {
        user_id: Main,
        page_size: limit,
        page: currentPage
      })
      // if (data.status) {
      setSalesData(data.sales_history.data)
      setTotalSales(data)
      setPageCount(Math.ceil(data.sales_history.total / limit))
      setLoading(false)
      // }
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    ApiExtract()
    SaleHistoryExtract()
  }, [currentPage])

  const handlePageClick = async (data) => {
    setCurrentPage(data.selected + 1)
  }
  return (
    <SalesLayout>
      <div className="saleHistoryProfile">
        <UserProfile UserDetail={UserDetail} loading={loading} />
        <div className="SalesHistory">
          <div className="Sales_Heading">
            <p className='SalesHeadline'>SALES HISTORY</p>
          </div>
          <>
            <div className="saleDate">
              <p>From {totalSales.months}</p>
            </div>
            <div className="PurchasedProductHead Saleshistorydata">
              PURCHASED PRODUCT
            </div>
            {loading ? <div className='LoadingBg'> <SpinnerDotted size={100} thickness={100} speed={100} color="rgba(0, 217, 255, 1)" /> </div> :
              SalesData.length?
              <div className="PurchasedProduct">
                <SalesHistoryProduct SalesData={SalesData} totalSales={totalSales} />
              </div> : <NoDataFound />}
          </>
          {SalesData.length ?  <PaginationTable pageCount={pageCount} handlePageClick={handlePageClick} /> : ""}
        </div>
      </div>
    </SalesLayout>
  )
}

export default SaleHistory