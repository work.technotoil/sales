
import './UseTable.css';

const UseTable = ({items}) => {
  return (
 
        
      <div className="page_content">
        <table className="table_body" >
          <thead>
            <tr>
              <th>Customer Code</th>
              <th>Customer Name</th>
              <th>Address</th>
              <th>Phone number</th>
            </tr>
          </thead>
          <tbody>
            {
              items.map((item) => {
                return (
                  <tr className='main' key={item.id}>
                    <td data-label="Customer Code">GTBBHCM001</td>
                    <td data-label="Customer Name"> TH Thúy Liễu </td>
                    <td data-label="Address"> {item.email} </td>
                    <td data-label="Phone number"> 9521362251 </td>
                    <td>
                      <button>view profile</button> </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>

      </div>

  )
}

export default UseTable;