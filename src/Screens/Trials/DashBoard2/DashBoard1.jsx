import { useEffect, useState } from 'react';
import './DashBoard1.css';
import UseTable from './UseTable/UseTable';
// import Arrow from '../../image/Polygon 14.svg';
import PaginationTable from './PaginationTable/PaginationTable';
// import DrawerMenu from '../Drawer/DrawerMenu';
import { useTranslation } from 'react-i18next';

const DashBoard = () => {
    const { t } = useTranslation()

    const [items, setItems] = useState([]);

    const [pageCount, setpageCount] = useState(0);

    let limit = 7;
    useEffect(() => {
        const getComments = async () => {
            const res = await fetch(
                `https://jsonplaceholder.typicode.com/comments?_page=1&_limit=${limit}`
            );
            const data = await res.json();
            const total = res.headers.get("x-total-count");
            setpageCount(Math.ceil(total / limit));
            setItems(data);
        };

        getComments();
    }, []);

    const fetchComments = async (currentPage) => {
        const res = await fetch(
            `https://jsonplaceholder.typicode.com/comments?_page=${currentPage}&_limit=${limit}`
        );
        const data = await res.json();
        return data;
    };

    const handlePageClick = async (data) => {
        let currentPage = data.selected + 1;

        const commentsFormServer = await fetchComments(currentPage);
        setItems(commentsFormServer);
    };

    return (

        <div className="dashboard_body">
            <div className="top_content">
                <div className="h_style1">
                    <div className=" drawer_menu">
                        {/* <div className='dmenu' ><DrawerMenu /> </div> &nbsp;&nbsp; */}
                       
                        <p>{t("DASHBOARD_SCREEN.CUSTOMER")} </p> &nbsp;&nbsp;
                        <p>{t("DASHBOARD_SCREEN.MANAGEMENT")}</p>
                        
                    </div>
                    <div className="UPbody">
                        <div className='UProf'>
                            <div className="UPfo">
                                <span>TÂN BÌNH</span>
                            </div>
                            <div className='arrow'>
                                <img src="" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="MainTable">
                <UseTable items={items} />
            </div>
            <div className="bottom_content"></div>
            <PaginationTable
                pageCount={pageCount}
                handlePageClick={handlePageClick} />
        </div>
    )
}

export default DashBoard;