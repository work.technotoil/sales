import Calendar from '../../../image/Calendar.svg'
import ProgressBar from '../../../Helper/ProgressBar';
// import 'antd/dist/antd.css';
import { CaretDownOutlined } from '@ant-design/icons';
import ToolTip from '../../../Helper/Tootip';
import "./OMTable.css"



const OMTable = ({ items,  togg, loading }) => {
  const SortTable=(e)=>{
  }
  return (
    // loading ? <div className='LoadingBg'> <SpinnerDotted size={100} thickness={100} speed={100} color="rgba(0, 217, 255, 1)" /> </div> :
      <div className="MainTable orderMOnitor">
        <table className="table_body4" >
          <thead>
            <tr>
              <th className='CustomerCode'>Order ID</th>
              <th className='CustomerCode'>Customer Code</th>
              <th className='CustomerName CenterText'>Customer Name</th>
              <th className='OrderDate'>Order Date <CaretDownOutlined onClick={SortTable} /></th>
              <th className='main' style={{ textAlign: "center" }}>Status</th>
              <th className='CustomerCode'></th>
            </tr>
          </thead>
          <tbody>
            <tr className='StatusMain' style={{ backgroundColor: '#E0FAFF' }}>
              <td className='td_tr_1'> </td>
              <td className='CustomerCode'> </td>
              <td className='td_tr_3'> </td>
              <td className='td_tr_4'> </td>
              <td className='MainTd'>
                <div className="StatusSection">
                  <div className='statusBar Submitted'>Submitted</div>
                  <div className='statusBar Approved'>Approved</div>
                  <div className='statusBar Delivering'>Delivering</div>
                  <div className='statusBar Delivered'>Delivered</div>
                  <div className='statusBar Recived'>Recieved</div>
                  <div className='statusBar Cancelled'>Cancelled</div>
                </div>
              </td>
              <td className='td_tr_10 OverDue'>Overdue (days)</td>
            </tr>

            {
              items.map((item) => {

                const {id , order_id , customer_code , user_name , order_date, order_type , overdue_days} = item
                return (

                  <tr className='main4 main' key={id}>
                    <td className='CustomerCode'>{order_id} </td>
                    <td className='CustomerCode'>{customer_code}</td>
                    <td className="CustomerName"> <p><ToolTip TitleText={user_name} ParaText={user_name} /></p></td>
                    <td className='OrderDate'><img src={Calendar} alt="" /> {order_date} </td>
                    {/* <td >  <ProgressBar Count={order_type >= 3 ? order_type - 1 : order_type} />  </td> */}
                    <td >  <ProgressBar Count={order_type } />  </td>
                    <td style={{textAlign:"center"}}> {overdue_days ? <div className="overdue_days">{overdue_days} </div> : ""} </td>
                  </tr>


                )
              })
            }
          </tbody>
        </table>

      </div>

  )
}

export default OMTable;