import { useEffect, useState } from 'react';
import './OdrMonitor.css';
import SalesLayout from '../../Layout/SalesLayout/SalesLayout';
import DropDown from '../../Helper/DropDown/DropDown';
import { all_orders, BaseUrl, get_area, get_route_name, get_route_no } from '../../API/BaseUrl';
import PaginationTable from '../Trials/DashBoard2/PaginationTable/PaginationTable';
import SearchInput from '../../Helper/SearchInput/Index';
import { SpinnerDotted } from 'spinners-react';
import NoDataFound from '../../Utils/NoDataFound/NoDataFound';
import ToolTip from '../../Helper/Tootip';
import { CaretDownOutlined } from '@ant-design/icons';
import ProgressBar from '../../Helper/ProgressBar';
import Calendar from "../../image/Calendar.svg"
import "./OMTable/OMTable.css"
import ToastMessage from '../../Helper/ToastMessage';
import { checkAvailable } from '../../Utils/helper';


let defaultobj = {
    value: "",
    label: "Area name"
}
let defaultobj1 = {
    value: "",
    label: "Route name"
}
let defaultobj2 = {
    value: "",
    label: "Route no."
}
const OdrMonitor = () => {
    const [items, setItems] = useState([]);
    const [loading, setLoading] = useState(false)
    const [pageCount, setPageCount] = useState(0)
    const [AreaId, setAreaId] = useState("")
    const [RouteNameId, setRouteNameId] = useState("")
    const [RouteNoId, setRouteNoId] = useState("")
    const [Search, setSearch] = useState("")
    const [currentPage, setCurrentPage] = useState(1)
    const [defaultVal, setDefaultVal] = useState(defaultobj)
    const [defaultVal1, setDefaultVal1] = useState(defaultobj1)
    const [defaultVal2, setDefaultVal2] = useState(defaultobj2)
    const [AreaId2, setAreaId2] = useState([])
    const [AreaName, setAreaName] = useState([])
    const [RouteNo, setRouteNo] = useState([])
    const [sortTogg, setSortTogg] = useState(false)
    const MainToken = localStorage.getItem("salesman_id")

    const salesman_id = localStorage.getItem("salesman_id")
    const areaIdApi = async () => {
        try {
            const { data } = await BaseUrl.post(get_area, {
                salesman_id: salesman_id
            })
            setAreaId2(data.area_list)
        } catch (err) {
            ToastMessage("danger", err.message)
        }
    }
    const routeNameApi = async (area) => {
        try {
            const { data } = await BaseUrl.post(get_route_name, {
                area_id: area,
                salesman_id: salesman_id
            })
            setAreaName(data.route_name_list)
        } catch (error) {
            ToastMessage("danger", error.message)
        }
    }
    const routeNoApi = async (route) => {
        try {
            const { data } = await BaseUrl.post(get_route_no, {
                route_name_id: route,
                salesman_id: salesman_id
            })
            setRouteNo(data.route_number_list)
        } catch {

        }
    }
    useEffect(() => {
        areaIdApi()
    }, [])
    let limit = 100;
    const ApiExtract = async () => {
        try {
            setLoading(true)

            const { data } = await BaseUrl.post(all_orders, {
                time_zone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                page_size: limit,
                page: currentPage,
                salesman_id: MainToken,
                area_name_id: AreaId,
                route_name_id: RouteNameId,
                route_no_id: RouteNoId,
                search: Search,
                column_name: "order_date",
                type: sortTogg ? "ASC" : "DESC",
            })
            if (data.status) {
                setItems(data.order_data.data)
                setPageCount(Math.ceil(data.order_data.total / limit))
            }

        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }

    useEffect(() => {
        ApiExtract()
    }, [AreaId, RouteNameId, RouteNoId, Search, currentPage, sortTogg]);

    const handlePageClick = async (data) => {
        setCurrentPage(data.selected + 1)
    }

    const onSelect = async (data, type) => {
        if (type === "area") {
            routeNameApi(data.value)
            setAreaId(data.value)
            setDefaultVal(data)
            setRouteNameId("")
            setRouteNoId("")
            setDefaultVal2(defaultobj2)
            setDefaultVal1(defaultobj1)
        } else if (type === "routename") {
            routeNoApi(data.value)
            setRouteNameId(data.value)
            setDefaultVal1(data)
            setDefaultVal2(defaultobj2)
            setRouteNoId("")
        } else {
            setRouteNoId(data.value)
            setDefaultVal2(data)
        }
    }
    const SearchText = (data, type) => {
        if (type === "customerName") {
            if (data.length > 2) {
                setSearch(data)
            } else {
                setSearch(data)
            }
        }
    }
    const handleClear = () => {
        setSearch('')
    }
    const onClickd = (data, type) => {
        if (type === "routename") {
            if (!AreaId) {
            }
        } else if (type === "routeNo") {
            if (!RouteNameId) {
            }
        }

    }
    const SortTable = (e) => {
        setSortTogg(!sortTogg)
    }
    return (
        <SalesLayout>
            <div className='Odrmonitor'>
                <div className="TableHead">
                    <div className="TableHeading Sales_Heading">
                        <p> ORDER MONITOR</p>
                    </div>
                    <div className="FilterDrop">
                        <div className="filterTop">
                            <ul>
                                <li><DropDown onClick={onClickd} RouteNameList={AreaId2} disabled={false} onSelect={onSelect} type="area" defaultVal={defaultVal} /> </li>
                                <li><DropDown RouteNameList={AreaName} onClick={onClickd} disabled={AreaId ? false : true} onSelect={onSelect} type="routename" defaultVal={defaultVal1} DropText="Route Name" /> </li>
                                <li><DropDown RouteNameList={RouteNo} onClick={onClickd} disabled={AreaId && RouteNameId ? false : true} onSelect={onSelect} type="routeNo" defaultVal={defaultVal2} DropText="Route Number" /></li>
                            </ul>
                        </div>
                        <div className="dropDownSearch">
                            <div className='searchandclear'>
                                <SearchInput func={handleClear} value={Search} placeholder="Search Customer Name..." SearchText={SearchText} type="customerName" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className='OrderCount'><p>{Search.length >= 3 ? `${items.length} order(s)` : ""}</p></div>
                {loading ? <div className='LoadingBg'> <SpinnerDotted size={100} thickness={100} speed={100} color="rgba(0, 217, 255, 1)" /> </div> :
                    items?.length ?
                        <>
                            <div className="mid_content1 odrMonitorTable">
                                <div className="MainTable orderMOnitor">
                                    <table className="table_body4" >
                                        <thead>
                                            <tr>
                                                <th className='CustomerCode'>Transaction ID</th>
                                                <th className='CustomerCode'>Customer Code</th>
                                                <th className='CustomerName CenterText'>Customer Name</th>
                                                <th className='OrderDate'>Order Date <CaretDownOutlined className={sortTogg ? 'changeIcon' : ""} name="order_date" onClick={SortTable} /></th>
                                                <th className='main' style={{ textAlign: "center" }}>Status</th>
                                                <th className='CustomerCode'></th>
                                                <th className='PaymentStatus'>Payment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className='StatusMain' style={{ backgroundColor: '#E0FAFF' }}>
                                                <td className='td_tr_1'> </td>
                                                <td className='CustomerCode'> </td>
                                                <td className='td_tr_3'> </td>
                                                <td className='td_tr_4'> </td>
                                                <td className='MainTd'>
                                                    <div className="StatusSection">
                                                        <div className='statusBar Submitted'>Submitted</div>
                                                        <div className='statusBar Approved'>Approved</div>
                                                        <div className='statusBar Delivering'>Delivering</div>
                                                        <div className='statusBar Delivered'>Delivered</div>
                                                        <div className='statusBar Cancelled'>Cancelled</div>
                                                    </div>
                                                </td>
                                                <td className='td_tr_10 OverDue'>Overdue (days)</td>
                                                <td ></td>

                                            </tr>

                                            {
                                                items.map((item) => {

                                                    const { id, customer_code, user_name, order_date, order_type, overdue_days, payment_status, transaction_id } = item
                                                    return (

                                                        <tr className='main4 main' key={id}>
                                                            <td className='CustomerCode'>{checkAvailable(transaction_id)} </td>
                                                            <td className='CustomerCode'>{checkAvailable(customer_code)}</td>
                                                            <td className="CustomerName custnameorder"> <p><ToolTip TitleText={user_name} ParaText={user_name} /></p></td>
                                                            <td className='OrderDate'><img src={Calendar} alt="" /> {order_date} </td>
                                                            <td><ProgressBar Count={order_type} />  </td>
                                                            <td style={{ textAlign: "center" }}>  {overdue_days ? <div className="overdue_days">{overdue_days} </div> : ""} </td>
                                                            <td className='PaymentStatus'>{payment_status}</td>
                                                        </tr>


                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </> : <NoDataFound />}
                {items?.length ? <PaginationTable
                    pageCount={pageCount}
                    handlePageClick={handlePageClick}
                /> : ""}
            </div>
            {/* <ToastContainer /> */}
        </SalesLayout>
    )
}

export default OdrMonitor;