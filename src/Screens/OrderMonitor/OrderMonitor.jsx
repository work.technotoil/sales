import { useEffect, useState } from 'react';
import './OrderMonitor.css';
import UseTableOrder from './UseTableOrder/UseTableOrder';
import SalesLayout from '../../Layout/SalesLayout/SalesLayout';
import { all_customers_list, BaseUrl } from '../../API/BaseUrl';
import PaginationTable from '../Trials/DashBoard2/PaginationTable/PaginationTable';
import DropDown from '../../Helper/DropDown/DropDown';
import Nodata from '../../Helper/Nodata';
import Loader from '../../Helper/Loader';

let limit = 100
const OrderMonitor = () => {
    const [items, setItems] = useState([]);
    const [loading, setLoading] = useState(false)
    const [pageCount, setpageCount] = useState(0)
    const [month, setMonth] = useState("")
    const [currentPage, setCurrentPage] = useState(1)
    const [filter, setFilter] = useState(false)
    const MonthFilter = [
        {
            id: 1,
            route_no: "1 month"
        },
        {
            id: 3,
            route_no: "3 months"
        }
    ]
    let defaultobj = {
        value: "",
        label: "Select month"
    }
    const [defaultVal, setDefaultVal] = useState(defaultobj)
    const MainToken = localStorage.getItem("salesman_id")
    const ApiExtract = async () => {
        try {
            setLoading(true)
            const { data } = await BaseUrl.post(all_customers_list, {
                page_size: limit,
                page: currentPage,
                month_type: month,
                salesman_id: MainToken,
                sort_by: filter ? 'ASC' : 'DESC'
            })
                setItems(data.customer_list.data)
                setpageCount(Math.ceil(data.customer_list.total / limit))

        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }
    useEffect(() => {
        ApiExtract()
    }, [month, currentPage, filter])


    const handleFilter = async () => {
        setFilter(!filter)
    }

    const handlePageClick = (data) => {
        setCurrentPage(data.selected + 1)
    }

    const onSelect = (data, type) => {
        if (type === "area") {
            setMonth(data.value)
            setDefaultVal(data)
        }
    }
    const onClick = (data, type) => {
    }
    return (
        <SalesLayout>
            <div className='OrderMonitor'>
                <div className="h_style1">
                    <div className="Sales_Heading">
                        <p>ORDER REPORT </p>
                    </div>
                    <div className="OrdermonitorFilter">
                        <DropDown onClick={onClick} DropText="Select..." onSelect={onSelect} defaultVal={defaultVal} RouteNameList={MonthFilter} type="area" />
                    </div>
                </div>
                <div className="orderMonitorTable">
                    {loading ? <Loader /> : items.length ?
                        <UseTableOrder loading={loading} items={items} func={handleFilter} /> :
                        <Nodata />}
                </div>
                {items.length ? <PaginationTable
                    pageCount={pageCount}
                    handlePageClick={handlePageClick}
                /> : ""}
            </div>
        </SalesLayout>
    )
}

export default OrderMonitor;