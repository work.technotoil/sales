
import { useNavigate } from 'react-router-dom';
import { SpinnerDotted } from 'spinners-react';
import './UseTableOrder.css';
import { order_monitor_sale_history } from '../../../API/urlconstantsales';
import Filtericon from '../../../image/Filtericon.svg'
import ToolTip from '../../../Helper/Tootip';
import { checkAvailable } from '../../../Utils/helper';


const UseTableOrder = ({ items, loading, func }) => {
  const navigate = useNavigate()
  const viewDetail = (item) => {
    const UserId = item.user_id
    navigate(order_monitor_sale_history, {
      state: {
        UserId
      }
    })
  }
  return (
    loading ? <div className='LoadingBg'> <SpinnerDotted size={100} thickness={100} speed={100} color="rgba(0, 217, 255, 1)" /> </div> :
      <table className="table_body" >
        <thead style={{ backgroundColor: '#E0FAFF' }}>
          <tr>
            <th className="CustomerCode ">Customer Code</th>
            <th className="CustomerName CenterText">Customer Name</th>
            <th className="CustomerAddr CenterText" >Address</th>
            <th className="CustomerPhone">Phone number</th>
            <th className="OrderDays daysfilter" onClick={func}>
              <p>No Order (Days)</p>
              <img src={Filtericon} alt='' />
            </th>
            <th className="ViewDetail">Details</th>
          </tr>
        </thead>
        <tbody className='OrderMonitorTb'>
          {
            items.map((item) => {
              const { user_id, customer_code, customer_name, address, mobile_no, no_order_days, dial_code } = item
              return (
                <tr className='OrderMonitorTr main' key={user_id}>
                  <td className="CustomerCode" data-label="Customer Code">{checkAvailable(customer_code)}</td>
                  <td className="CustomerName" data-label="Customer Name"> <ToolTip TitleText={customer_name} ParaText={customer_name} /></td>
                  <td className="reportaddr CustomerAddr Address" data-label="Address"> <p><ToolTip TitleText={address} ParaText={address} /></p> </td>
                  <td className="CustomerPhone" data-label="Phone number"> {dial_code ? `${dial_code} ${mobile_no}` : `${dial_code}${mobile_no}`} </td>
                  <td className="OrderDays orderDaysList" data-label="Phone number"> {no_order_days ? no_order_days : 0} </td>
                  <td className='ViewDetail'> <span onClick={() => { viewDetail(item) }}>View</span></td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
  )
}

export default UseTableOrder;