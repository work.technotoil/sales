import { useEffect, useState } from 'react';
import './DashBoard.css';
import UseTable from './UseTable/UseTable';
import SalesLayout from '../../Layout/SalesLayout/SalesLayout';
import { BaseUrl, customer_list, get_area, get_route_name, get_route_no } from '../../API/BaseUrl';
import PaginationTable from '../Trials/DashBoard2/PaginationTable/PaginationTable';
import DropDown from '../../Helper/DropDown/DropDown';
import SearchInput from '../../Helper/SearchInput/Index';
import { useDispatch, useSelector } from 'react-redux';
import { setAreaId, setAreaName, setRouteNo } from '../../redux/action';
import { SpinnerDotted } from 'spinners-react';
import Nodata from '../../Helper/Nodata';
let limit = 100;
const DashBoard = () => {
    const [items, setItems] = useState([]);
    const [pageCount, setpageCount] = useState(0);
    const [loading, setLoading] = useState(false)
    const [AreaIdno, setAreaIdno] = useState()
    const dispatch = useDispatch()
    const [currentPage, setCurrentPage] = useState(1)
    const [searchLen, setSearchLen] = useState('')
    const AreaId2 = useSelector(state => state.AreaId)
    const MainToken = localStorage.getItem("salesman_id")
    const [SearchCount, setSearchCount] = useState()
    const [customerCount , setCustomerCount] = useState("")


    const ApiExtract = async () => {
        try {
            setLoading(true)
            const { data } = await BaseUrl.post(customer_list, {
                page_size: limit,
                page: currentPage,
                salesman_id: MainToken,
                area_name_id: AreaIdno,
                search: searchLen.length >= 3 ? searchLen : ""
            })
            setCustomerCount(data.customer_count)
            if (data.customer_list.total === data.customer_count) {
                setSearchCount("")
            } else if (data.customer_list.total - data.customer_count === 0) {
                setSearchCount("No result(s)")
            } else {
                setSearchCount(`${data.customer_list.total} result(s)`)
            }
            setItems(data.customer_list.data)
            setpageCount(Math.ceil(data.customer_list.total / limit))

        } catch (error) {
        } finally {
            setLoading(false)
        }
    }
    const handleClear = () => {
        setSearchLen('')
    }

    const SearchText = (data, type) => {

        if (data.length >= 3) {
            setSearchLen(data)

        } else {
            setSearchLen(data)
        }
    }


    useEffect(() => {
        ApiExtract()
    }, [AreaIdno, searchLen.length >= 3 ? searchLen : "", currentPage])


    const ApiExtract2 = async () => {
        const AreaId = await BaseUrl.post(get_area,
            { salesman_id: MainToken, })
        dispatch(setAreaId(AreaId.data.area_list))
        const AreaName = await BaseUrl.post(get_route_name)
        dispatch(setAreaName(AreaName.data.route_name_list))
        const RouteNo = await BaseUrl.post(get_route_no)
        dispatch(setRouteNo(RouteNo.data.route_number_list))
    }
    useEffect(() => {
        ApiExtract2()
    }, [])

    const handlePageClick = async (data) => {
        setCurrentPage(data.selected + 1)
    }
    const onSelect = (data, type) => {
        if (type === "area") {
            setAreaIdno(data.value)
        }
    }
    const onClick = (data, type) => {
        console.log("check area dropdown", data, type)
    }
    return (
        <SalesLayout>
            <div className="dashboard_body">
                <div className="top_content">
                    <div className="h_style1">
                        <div className="Sales_Heading">
                            <p>CUSTOMER MANAGEMENT
                            {customerCount? ` ( ${customerCount} )` : ""}
                            </p>
                        </div>

                        <div className="UPbody">
                            <div className="searchandclear">
                                <SearchInput func={handleClear} value={searchLen} placeholder="Search..." SearchText={SearchText} />
                            </div>
                            <DropDown onClick={onClick} DropText="Select area" onSelect={onSelect} RouteNameList={AreaId2} type="area" defaultDrop="Select area" />
                        </div>
                    </div>
                </div>
                <div className='searchdashboard'>
                    {SearchCount}
                    
                </div>

                {loading ? <div className='LoadingBg'> <SpinnerDotted size={100} thickness={100} speed={100} color="rgba(0, 217, 255, 1)" /> </div> :
                    items.length ? <>
                        <div className="CustomerManagement">
                            <UseTable items={items} loading={loading} />
                        </div>

                    </> : <Nodata />}
                {items.length ? <PaginationTable
                    pageCount={pageCount}
                    handlePageClick={handlePageClick} /> : ""}
            </div>
            {/* <ToastContainer /> */}
        </SalesLayout>
    )
}

export default DashBoard;