
import './PaginationTable.css';
import ReactPaginate from "react-paginate";
import PrevVector from '../../../image/PrevVector.svg';
import NextVector from '../../../image/NextVector.svg';

const Prev = () => {
  return (
    <img src={PrevVector} alt="" />
  )
}
const Next = () => {
  return (
    <img src={NextVector} alt="" />
  )
}

const PaginationTable = ({pageCount, handlePageClick}) => {
  return (
    <div>
      
      <ReactPaginate
        breakLabel={"..."}
        previousLabel={< Prev />}
        nextLabel={< Next />}
        pageCount={pageCount}
        search = {""}
        marginPagesDisplayed={2}
        pageRangeDisplayed={3}
        onPageChange={handlePageClick}
        pageClassName={"page-item"}
        pageLinkClassName={"page-link"}
        previousClassName={"page-item"}
        nextClassName={"page-item"}
        breakClassName={"page-item"}
        breakLinkClassName={"page-link"}
        containerClassName={"paginationBttns"}
        previousLinkClassName={"previousBttn"}
        nextLinkClassName={"nextBttn"}
        disabledClassName={"paginationDisabled"}
        activeClassName={"paginationActive"}
      />
    </div>
  )
}

export default PaginationTable;