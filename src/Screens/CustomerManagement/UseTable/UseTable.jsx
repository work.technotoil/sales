import './UseTable.css';
import { useNavigate } from "react-router-dom"
import { SpinnerDotted } from 'spinners-react';
import "../../../Components/CommonTable.css"
import NotAvailable from '../../../Helper/NotAvailable';
import { sale_history } from '../../../API/urlconstantsales';
import ToolTip from '../../../Helper/Tootip';
import { checkAvailable } from '../../../Utils/helper';
const UseTable = ({ items, loading }) => {

  const navigate = useNavigate()
  const ViewDetail = (item) => {
    const UserId = item.user_id
    navigate(sale_history, {
      state: {
        UserId
      }
    })
  }
  return (
    <div className="page_content">
      <table className="table_body" >
        <thead>
          <tr>
            <th className="CustomerCode">Customer Code</th>
            <th className="CustomerName CenterText">Customer Name</th>
            <th className='CustomerAddr CenterText'>Address</th>
            <th className='CustomerPhone'>Phone number</th>
            <th className='CustomerPhone'></th>
          </tr>
        </thead>
        <tbody>

          {
            items.map((item, index) => {
              const { customer_name, address } = item
              return (
                <tr className='main' key={index}>
                  <td className='CustomerCode' data-label="Customer Code">{checkAvailable(item.customer_code)}</td>
                  <td className='CustomerName' data-label="Customer Name"> {item.customer_name ? <ToolTip TitleText={customer_name} ParaText={customer_name} /> : <NotAvailable />} </td>            
                  <td className=' addrTd CustomerAddr Address' data-label="Address"> <p>{item.address ? <ToolTip TitleText={address} ParaText={address} /> : <NotAvailable />}</p> </td>
                  <td className='CustomerPhone' data-label="Phone number"> {item.mobile_no ? item.dial_code ? (`${item.dial_code} ${item.mobile_no}`) : (`${item.dial_code}${item.mobile_no}`) : <NotAvailable />} </td>                  
                  <td className='ViewDetail'>
                    <button onClick={() => { ViewDetail(item) }}>view profile</button> </td>
                </tr>
              )
            })
          }
        </tbody>
      </table>

    </div>

  )
}

export default UseTable;