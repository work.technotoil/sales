import React, { useEffect } from 'react'
import { useState } from 'react'
import Accordion from '../../Components/Accordion/Index'
import SalesLayout from '../../Layout/SalesLayout/SalesLayout'
import DropDown from "../../Helper/DropDown/DropDown"
import EYE from "../../image/EYE.png"
import "./Index.css"
import { BaseUrl, category_group_product, customer_detail_order_detail, get_area, get_route_name, get_route_no } from '../../API/BaseUrl'
import PaginationTable from '../Trials/DashBoard2/PaginationTable/PaginationTable'
import Loader from '../../Helper/Loader'

import Nodata from '../../Helper/Nodata'
import { excelEportIcon, Nodatas } from '../../Helper/ImagePath'
import ModalPopup from '../../Components/ModalPopup/Index'
import LoginBtn from '../../LoginComponent/LoginBtn/LoginBtn'
import ReactHtmlTableToExcel from 'react-html-table-to-excel'
import { checkAvailable } from '../../Utils/helper'

let limit = 100

let defaultobj = {
    value: "",
    label: "Area name"
}
let defaultobj1 = {
    value: "",
    label: "Route name"
}
let defaultobj2 = {
    value: "",
    label: "Route no."
}
const DailyCallCard = () => {
    const [pageCount, setpageCount] = useState(0)
    const [apiRes, setApiRes] = useState([])
    const [loading, setLoading] = useState(false)
    const [ModalMain, setModalMain] = useState([])
    const [AllProductId, setAllProductId] = useState({})
    const [currentPage, setCurrentPage] = useState(1)
    const [refresh, setRefresh] = useState(false)
    const [isModalOpen, setIsModalOpen] = useState(false);
    const MainToken = localStorage.getItem("salesman_id")
    const salesman_name = localStorage.getItem("salesman_name")
    const [defaultVal, setDefaultVal] = useState(defaultobj)
    const [defaultVal1, setDefaultVal1] = useState(defaultobj1)
    const [defaultVal2, setDefaultVal2] = useState(defaultobj2)
    const [todaydate, setTodaydate] = useState()
    const [Areadrop, setAreadrop] = useState([])
    const [AreaIdsel, setAreaIdsel] = useState("")
    const [AreaNamesel, setAreaNamesel] = useState([])
    const [RouteNameId, setRouteNameId] = useState("")
    const [RouteNoIdsel, setRouteNoIdsel] = useState("")
    const [RouteNosel, setRouteNosel] = useState([])
    const salesman_id = localStorage.getItem("salesman_id")

    const AreaIdApi = async () => {
        const AreaIdconst = await BaseUrl.post(get_area,{
            salesman_id : salesman_id
        })
        setAreadrop(AreaIdconst.data.area_list)
    }

    useEffect(() => {
        AreaIdApi()
    }, [])

    const ApiExtract = async () => {
        try {
            setLoading(true)
            const { data } = await BaseUrl.post(customer_detail_order_detail, {
                page_size: limit,
                timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                page: currentPage,
                salesman_id: MainToken,
                area_name_id: AreaIdsel,
                route_name_id: RouteNameId,
                route_no_id: RouteNoIdsel,
                group_product_id: allProductId
            })
            if (data.status) {
                setTodaydate(data.today_date)
                setpageCount(Math.ceil(data.customer_list.total / limit))
                setApiRes(data.customer_list.data)
            }
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }
    useEffect(() => {
        ApiExtract()
    }, [AreaIdsel, RouteNameId, RouteNoIdsel, refresh, currentPage])

    const handlePageClick = async (data) => {
        setCurrentPage(data.selected + 1);

    }
    const ProductGroup = async () => {
        const { data } = await BaseUrl.post(category_group_product)
        const NewArray = data.CategoryAndGroupProduct
        NewArray.map((item) => {
            item.checked = false
            item.group_product.map((val) => {
                val.selected = false
            })
        })
        setModalMain(NewArray)
    }

    const showModal = () => {
        setIsModalOpen(true);
    };
    const handleOk = () => {
        setIsModalOpen(false);
    };
    const handleCancel = () => {
        setIsModalOpen(false);
    };

    useEffect(() => {
        ProductGroup()
    }, [])

    const onClickd = (data, type) => {
        if (type === "RouteName") {
        } else if (type === "RouteNo") {
        }
    }
    const FilterData = () => {
        const Filterarr = [...ModalMain]
        let mainData
        let newString = ""
        const data = Filterarr.map((item) => {
            mainData = item.group_product.filter((val) => {
                if (val.selected) {
                    newString = newString + "," + val.id
                } else {
                    return val
                }
            })
            return mainData
        })
        return { state: data, api: newString }
    }

    const SaveFilter = () => {
        setRefresh(!refresh)
        setIsModalOpen(false);
        setAllProductId(maintry.api.substring(1))
    }
    const onChange1 = (e, item, id) => {
        const newArray = ModalMain.map(a => ({ ...a }))
        newArray.forEach((val, index) => {
            if (id === val.id) {
                val.checked = !val.checked
                val.group_product.forEach((ele) => {
                    if (val.checked) {
                        ele.selected = true
                    } else {
                        ele.selected = false
                    }
                })
            }
        })
        setModalMain(newArray)
    }
    const CheckOnchange = (e, item, id) => {
        const newData = [...ModalMain]
        newData.forEach((todo) => {
            todo.group_product.forEach((list) => {
                if (list.id === id) {
                    list.selected = !list.selected
                } else {

                }
            })
        })
        setModalMain(newData)
    }
    const maintry = FilterData()
    let allProductId = maintry.api.substring(1)


    const onSelect = async (data, type) => {

        if (type === "area") {
            setAreaIdsel(data.value)
            setDefaultVal(data)
            try {
                const AreaNameconst = await BaseUrl.post(get_route_name, {
                    area_id: data.value,
                    salesman_id : salesman_id
                })
                setAreaNamesel(AreaNameconst.data.route_name_list)
            } catch (error) {
                alert(error.message)
            }
            setDefaultVal1(defaultobj1)
            setDefaultVal2(defaultobj2)
            setRouteNameId("")
            setRouteNoIdsel("")
        } else if (type === "RouteName") {

            setRouteNameId(data.value)
            try {
                const RouteNoconst = await BaseUrl.post(get_route_no, {
                    route_name_id: data.value,
                    salesman_id : salesman_id
                })
                setRouteNosel(RouteNoconst.data.route_number_list)
            } catch (error) {
                alert(error.message)
            }
            setDefaultVal1(data)
            setDefaultVal2(defaultobj2)
            setRouteNoIdsel("")
        } else {
            setRouteNoIdsel(data.value)
            setDefaultVal2(data)
        }
    }



    return (
        <SalesLayout>
            <div className="dailyCallHead">
                <div className="dailyCallTitle">
                    <div className="dailyP Sales_Heading">
                        <p className='Headmain'>DAILY CALL CARD</p>
                        <h5 className='HeadSecond'>ROUTING & SALES HISTORY</h5>
                    </div>

                    <div className="Export">
                        <p><span><img src={excelEportIcon} alt="" /></span><span><u>
                            <ReactHtmlTableToExcel
                                id="test-table-xls-button"
                                className="download-table-xls-button"
                                table="table-to-xls"
                                filename="Daily call card"
                                fileExtension="xlsx"
                                sheet="tablexls"
                                buttonText="Export to Excel" />
                        </u></span></p>
                        <ModalPopup
                            isModalOpen={isModalOpen} showModal={showModal} handleOk={handleOk} handleCancel={handleCancel}
                            BtnText={<p><span><img className='eyeiconcard' src={EYE} alt="" /></span><span><u>Product Group </u></span></p>}>
                            <div className="modalpopupList">
                                {ModalMain.map((item, index) => {
                                    return (
                                        <div className="Modalheading">
                                            <h5> <input type="checkbox" id={item.id} value={item.id} checked={item.checked} name={item.category_name} onChange={(e) => onChange1(e, item, item.id)} />{item.category_name} </h5>
                                            <div className="ModalItem">
                                                <ul>
                                                    {item.group_product.map((val, index) => {
                                                        return (
                                                            val.group_product_name ?
                                                                <li>
                                                                    <input type="checkbox" checked={val.selected} onChange={(e) => CheckOnchange(e, val, val.id)} value={val.id} />
                                                                    <p>{val.group_product_name}</p>
                                                                </li> : ""

                                                        )
                                                    })}
                                                </ul>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>

                            <LoginBtn disable={allProductId.length ? false : true} BtnText="Save" onClick={SaveFilter} />

                        </ModalPopup>
                    </div>

                </div>
                <div className="dailyCallFilter">

                    <div className="DropDownItems selectDrop">
                        <div className='AreaDropDown'>
                            <span>Area: </span> <DropDown
                                defaultVal={defaultVal}
                                onClick={onClickd}
                                RouteNameList={Areadrop}
                                disabled={false}
                                onSelect={onSelect}
                                type="area"
                                DropText="Select"
                            />
                        </div>
                        <div className='AreaDropDown'>
                            <span>Route Name: </span>
                            <DropDown
                                defaultVal={defaultVal1}
                                onClick={onClickd}
                                disabled={AreaIdsel ? false : true}
                                RouteNameList={AreaIdsel ? AreaNamesel : []}
                                onSelect={onSelect}
                                type="RouteName"
                                DropText="Select"
                            />
                        </div>
                        <div className='AreaDropDown'>
                            <span>Route No: </span>
                            <DropDown
                                defaultVal={defaultVal2}
                                onClick={onClickd}
                                disabled={AreaIdsel && RouteNameId ? false : true}
                                RouteNameList={RouteNameId ? RouteNosel : ""}
                                onSelect={onSelect}
                                type="RouteNo"
                                DropText="Select" />
                        </div>
                    </div>
                    {/* </div> */}
                    <div className="filterBottom">
                        <ul>
                            <li>Date: <span>{checkAvailable(todaydate)}</span></li>
                            <li>Salesman: <span className='SalesmanDaily'>{checkAvailable(salesman_name)}</span></li>
                        </ul>
                    </div>
                </div>
            </div>

            {loading ? <Loader /> : apiRes.length ?
                <div className='DailyCall'>
                    <div className='salesKpeTableHead DailyCallTable'>
                        <table id="table-to-xls" className='salesKpiTable DailyCallTable MainTable'>
                            <thead>
                                <tr className='accordionHead'>
                                    <th className='NumberSales'>No.</th>
                                    <th className='CustomerCode CustCode'>Customer Code</th>
                                    <th className='CustomerName CName CenterText'>Customer Name</th>
                                    <th className='CustomerAddr CenterText cAddress '>Address</th>
                                    <th className='CustomerPhone cPhone'>Phone number</th>
                                </tr>
                            </thead>
                            <tbody>
                                <Accordion apiRes={apiRes} allProductId={AllProductId} />
                            </tbody>
                        </table>
                    </div>

                </div> : <Nodata SRC={Nodatas} />}
            {apiRes.length ? <PaginationTable pageCount={pageCount} handlePageClick={handlePageClick} /> : ""}
        </SalesLayout>
    )
}

export default DailyCallCard
