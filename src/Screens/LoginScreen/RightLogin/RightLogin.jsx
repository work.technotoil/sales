/* eslint-disable no-restricted-globals */
/* eslint-disable no-unused-expressions */

import React, { useEffect, useState } from 'react'

import { Link, useNavigate } from 'react-router-dom'
import { useTranslation } from 'react-i18next';
import "./RightLogin.css"
// import { BaseUrl, SignIn } from '../../API/BaseUrl'
import LoginInput from '../../../LoginComponent/LoginInput/LoginInput';
import ErrorText from '../../../LoginComponent/ErrorText/ErrorText';
import LoginBtn from '../../../LoginComponent/LoginBtn/LoginBtn';
import LoginPageLayout from '../../../LoginLayout/LoginPageLayout/LoginPageLayout';
import { BaseUrl, login_sales } from '../../../API/BaseUrl';
import asyncLocalStorage from '../../../Helper/asyncLocalStorage';
import OpenEye from '../../../Helper/OpenEye';
import CloseEye from '../../../Helper/CloseEye';
import ToastMesage from "../../../Helper/ToastMessage.jsx"
import { ToastContainer } from 'react-toastify';
import { right_login, dashboard } from '../../../API/urlconstantsales';



const RightLogin = ({ setLoadingApp }) => {
  const Navigate = useNavigate()
  const [email, setEmail] = useState(null)
  const [formatedValue, setformatedValue] = useState('')
  const [password, setPassword] = useState('')
  const [loading, setloading] = useState(false)
  const [eyeImage, setEyeImage] = useState(false)
  const [showPassword, setShowPassword] = useState(false)

  const [Error, setError] = useState({
    email: false,
    emailError: "",
    password: false,
    passwordError: ""
  })
  const { t } = useTranslation();
  useEffect(() => {
    const token = localStorage.getItem("salesman_id")
    location.pathname === right_login && token ? Navigate(dashboard) : location.pathname
  }, [])
  const onSubmit = async (e) => {
    e.preventDefault()
    setLoadingApp(true)
    try {
      setloading(true)
      email == null && setError(prevState => ({ ...prevState, email: true, emailError: t('This feild is required') }))
      !password.length >= 1 && setError(prevState => ({ ...prevState, password: true, passwordError: t('This feild is required') }))
      if (!Error.email && !Error.password && email.length >= 1 && password.length >= 1) {

        const result = await BaseUrl.post(login_sales, {
          email: email,
          password: password
        })
        if (result.data.status === true) {
          // setloading(false)  
          await asyncLocalStorage.setItem("token", result.data.email_id);
          await asyncLocalStorage.setItem("salesman_id", result.data.salesman_id);
          await asyncLocalStorage.setItem("salesman_name", result.data.salesman_name);
          Navigate(dashboard)
          ToastMesage('success', result.data.message)
        }
        else {
          ToastMesage('danger', result.data.message)
        }
      }

    } catch (error) {
      setloading(false)
    } finally {
      setLoadingApp(false)
      setloading(false)
    }
  }
  const callBack = (value, type, formatedValue) => {
    if (type === 'email') {
      const EmailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      if (EmailPattern.test(value)) {
        setEmail(value)
        setformatedValue(formatedValue)
        setError(prevState => ({ ...prevState, email: false, emailError: "" }))
      }
      else if (value.length === 0) {
        setEmail(value)
        setError(prevState => ({ ...prevState, email: true, emailError: t('This feild is required ') }))
      }
      else {
        setEmail(value)
        setError(prevState => ({ ...prevState, email: true, emailError: 'Please Enter valid email ' }))
      }
    }

    else {
      setPassword(value)
      if (value.length >= 6) {
        setError(prevState => ({ ...prevState, password: false, passwordError: "" }))
      }
      else if (value.length === 0) {
        setPassword(value)
        setError(prevState => ({ ...prevState, password: true, passwordError: t("This feild is required") }))
      }
      else {
        setPassword(value)
        setError(prevState => ({ ...prevState, password: true, passwordError: "Password should be atleast 6 digit" }))
      }
    }
  }
  const ChangeImage = () => {
    setEyeImage(!eyeImage)
  }

  const PasswordShow = () => {
    setShowPassword(!showPassword)
  }

  return (
    <LoginPageLayout>
      <form className="RightLoginFeild" onSubmit={onSubmit}>

        <div className="login_head">
          <p> Welcome to <span>ShopFast</span> </p>
        </div>

        <div className='RightLoginItem'>
          <LoginInput Label={"Email"}
            Placeholder={"Enter your Email here"}
            type={email}
            callBack={callBack}
            fieldType='email'
            error={Error.email}
          />
          {Error.email ? <ErrorText error={Error.emailError} /> : null}
        </div>


        <div className='RightLoginItem'>
          <LoginInput Label={"Password"}
            Placeholder={"Enter your Password here"}
            type={showPassword ? 'text' : 'password'}
            value={password}
            {...{ callBack }}
            fieldType='password'
            error={Error.password}
          />
          <span onClick={ChangeImage} >{eyeImage ? < OpenEye onClick={PasswordShow} /> : < CloseEye onClick={PasswordShow} />}</span>
          {Error.password ? <ErrorText error={Error.passwordError} /> : null}
        </div>
        <div className="forgot">
          <p className='forgot_style'> <Link to={"/forgotpassword"}> Forgot Password?</Link> </p>
        </div>
        <div className='RightLoginItem'>
          <LoginBtn BtnText={"LOG IN"} loading={loading} />
        </div>


      </form>
      <ToastContainer />

    </LoginPageLayout>
  )
}

export default RightLogin
