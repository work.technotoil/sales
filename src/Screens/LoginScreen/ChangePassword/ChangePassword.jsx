import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useLocation, useNavigate } from "react-router-dom";
import { BaseUrl, forget_password } from "../../../API/BaseUrl";
import CloseEye from "../../../Helper/CloseEye";
import OpenEye from "../../../Helper/OpenEye";
import ErrorText from "../../../LoginComponent/ErrorText/ErrorText";
import LoginBtn from "../../../LoginComponent/LoginBtn/LoginBtn";
import LoginInput from "../../../LoginComponent/LoginInput/LoginInput";
import LoginPageLayout from "../../../LoginLayout/LoginPageLayout/LoginPageLayout";
import { right_login } from "../../../API/urlconstantsales";


const ChangePassword = () => {
  const navigate = useNavigate()
  const location = useLocation();
  const [password1, setPassword1] = useState("")
  const [password2, setPassword2] = useState("")
  const [loading, setloading] = useState(false)
  const [eyeImage, setEyeImage] = useState(false)
  const [showPassword, setShowPassword] = useState(false)
  const [eyeImage2, setEyeImage2] = useState(false)
  const [showPassword2, setShowPassword2] = useState(false)
  const [Error, setError] = useState({
    password1: false,
    password1Error: "",
    password2: false,
    password2Error: ""
  })

  const { t } = useTranslation();


  const ChangeImage = () => {
    setEyeImage(!eyeImage)
  }
  const PasswordShow = () => {
    setShowPassword(!showPassword)
  }
  const ChangeImage2 = () => {
    setEyeImage2(!eyeImage2)
  }
  const PasswordShow2 = () => {
    setShowPassword2(!showPassword2)
  }

  const onSubmit = async (e) => {
    e.preventDefault()
    try {
      setloading(true)
      password2 !== password1 && setError(prevState => ({ ...prevState, password2: true, password2Error: t("Password and confirm password should be same") }))
      !password1.length >= 1 && setError(prevState => ({ ...prevState, password1: true, password1Error: t("This feild is required") }))
      !password2.length >= 1 && setError(prevState => ({ ...prevState, password2: true, password2Error: t("This feild is required") }))
      if (!Error.password1 && !Error.password2 && password1.length >= 1 && password2.length >= 1 && password1 === password2) {   //checking all required fields


        const result = await BaseUrl.post(forget_password, {
          email: location.state.EmailSend,
          password: password1,
        })
        if (result.data.status === true) {
          setloading(false)
          navigate(right_login)
        }
        else {
          setloading(false)
        }
      }
      else {
        setloading(false)
      }
    } catch (error) {
      setloading(false)
    }
  }

  const callBack = (value, type) => {
    if (type === "password1") {
      setPassword1(value)
      if (value.length >= 6) {
        setError(prevState => ({ ...prevState, password1: false, password1Error: "" }))
      }
      else if (value.length === 0) {
        setPassword1(value)
        setError(prevState => ({ ...prevState, password1: true, password1Error: t("This feild is required") }))
      }
      else {
        setPassword1(value)
        setError(prevState => ({ ...prevState, password1: true, password1Error: t("Password should be atleast 6 digit") }))
      }
    }
    else {
      if (value === password1) {

        setPassword2(value)
        setError(prevState => ({ ...prevState, password2: false, password2Error: "" }))
      }
      else if (value.length === 0) {
        setPassword2(value)
        setError(prevState => ({ ...prevState, password2: true, password2Error: t("This feild is required") }))
      }
      else {
        setPassword2(value)
        setError(prevState => ({ ...prevState, password2: true, password2Error: t("Password and confirm password should be same") }))
      }
    }
  }

  return (
    <LoginPageLayout>
      <form className="forgot_password" onSubmit={onSubmit}>
        <div className="forgot_heading">
          <p className='heading_change'> CHANGE PASSWORD </p>
        </div>

        <div style={{ position: "relative" }} className='forgot_form'>
          <LoginInput type={showPassword ? "text" : "password"} Label={" Password"}
            Placeholder={"Enter your  Password here"}
            value={password1}
            callBack={callBack}
            fieldType='password1'
            error={Error.password1} />
            <span onClick={ChangeImage} >{eyeImage ? < OpenEye onClick={PasswordShow} /> : < CloseEye onClick={PasswordShow} />}</span>
          {/* <span onClick={ChangeImage} >{eyeImage ? < OpenEye onClick={PasswordShow} /> : < CloseEye onClick={PasswordShow} />}</span> */}
          {/* {password1.length > 0 ? <PasswordStrengthBar style={{
=======
          {/* <span onClick={"ChangeImage"} >{eyeImage ?" < OpenEye onClick={PasswordShow} />" : "< CloseEye onClick={PasswordShow} />"}</span>
          {password1.length > 0 ? 
          <PasswordStrengthBar style={{
>>>>>>> 2a5bcb7ec4e8bcbc3be97a9694d59205528986b1
            position: "absolute", maxWidth: "380px", bottom: "-30px", width: "95%",
            left: 0,
            right: 0,
            margin: "-5px auto"
          }} password={password1} /> : null} 
          {Error.password1 ? <ErrorText error={Error.password1Error} /> : null} */}
        </div>

        <div className='forgot_form'>
          <LoginInput type={showPassword2 ? "text" : "password"} Label={"Confirm Password"} Placeholder={"Enter your Confirm Password here"} value={password2} callBack={callBack} fieldType='password2' error={Error.password2} />
          <span onClick={ChangeImage2} >{eyeImage2 ? < OpenEye onClick={PasswordShow2} /> : < CloseEye onClick={PasswordShow2} />}</span>
          {Error.password2 ? <ErrorText error={Error.password2Error} /> : null}
        </div>
        <div className='forgot_form'>
          <LoginBtn BtnText="SAVE CHANGES" loading={loading} />
        </div>
        {Error.type === "overall" ? <ErrorText error={Error} /> : null}
      </form>
    </LoginPageLayout>
  )
}

export default ChangePassword;