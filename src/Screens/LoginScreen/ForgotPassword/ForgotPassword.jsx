

import { useState } from "react"
import LoginPageLayout from "../../../LoginLayout/LoginPageLayout/LoginPageLayout"
import LoginInput from '../../../LoginComponent/LoginInput/LoginInput';
import ErrorText from '../../../LoginComponent/ErrorText/ErrorText';
import LoginBtn from '../../../LoginComponent/LoginBtn/LoginBtn';
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import "./ForgotPassword.css"
import { BaseUrl, send_otp } from "../../../API/BaseUrl";
import ToastMessage from "../../../Helper/ToastMessage";
import { ToastContainer } from "react-toastify";
import { verify_otp } from "../../../API/urlconstantsales";

const ForgotPassword = () => {
    const [email, setEmail] = useState(null)
    const [loading, setloading] = useState(false)
    const [Error, setError] = useState({
        email: false,
        emailError: "",
    })
    const Navigate = useNavigate()



    const { t } = useTranslation();

    const onSubmit = async (e) => {
        e.preventDefault()
        // Navigate("/VerifyOtp")
        try {
            setloading(true)
            email == null && setError(prevState => ({ ...prevState, email: true, emailError: t("This feild is required") }))

            if (!Error.email && email.length >= 1) {
                const result = await BaseUrl.post(send_otp, {
                    email: email,
                })
                if (result.data.status === true) {
                    if (result.data.is_verify === 0) {
                        const result = await BaseUrl.post(send_otp, {
                            email: email,
                        })
                        const NewOtp = result.data.otp.toString()
                        ToastMessage("success", result.data.message)
                        Navigate(verify_otp, {
                            state: {
                                email,
                                NewOtp
                            }
                        })

                    } else {
                        setloading(false)
                        const NewOtp = result.data.otp.toString()
                        ToastMessage("success", result.data.message)
                        Navigate(verify_otp, {
                            state: {
                                email,
                                NewOtp
                            }
                        })
                    }
                }
                else {
                    ToastMessage("danger", result.data.message)
                    setloading(false)
                }

            } else {
                setloading(false)
                alert()
            }

        } catch (error) {
            setloading(false)
        } finally {
            setloading(false)
        }
    }
    const callBack = (value, type) => {
        if (type === 'email') {
            const EmailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            if (EmailPattern.test(value)) {
                setEmail(value)
                // setformatedValue(formatedValue)
                setError(prevState => ({ ...prevState, email: false, emailError: "" }))
            }
            else if (value.length === 0) {
                setEmail(value)
                setError(prevState => ({ ...prevState, email: true, emailError: t('This feild is required ') }))
            }
            else {
                setEmail(value)
                setError(prevState => ({ ...prevState, email: true, emailError: t('Please Enter valid email ') }))
            }
        } else if (value.length === 0) {
            setEmail(value)
            setError(prevState => ({ ...prevState, email: true, emailError: t('This feild is required') }))
        }


    }
    return (
        <LoginPageLayout>
            <form className="RightLoginFeild" onSubmit={onSubmit}>
                <div className="login_head">
                    <p className='heading_top'> Forgot Password </p>
                </div>

                <div className='RightLoginItem'>
                    <LoginInput Label={"Email"}
                        Placeholder={"Please Enter your Email here"}
                        type={email}
                        callBack={callBack}
                        fieldType='email'
                        error={Error.email}
                    />
                    {Error.email ? <ErrorText error={Error.emailError} /> : null}
                </div>
                <div className='RightLoginItem'>
                    <LoginBtn BtnText={"NEXT"} loading={loading} />
                </div>
            </form>
            <ToastContainer />

        </LoginPageLayout>
    )
}
export default ForgotPassword;
