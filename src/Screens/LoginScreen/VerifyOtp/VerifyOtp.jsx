import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useLocation, useNavigate } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { BaseUrl, send_otp, verify_otp } from "../../../API/BaseUrl";
import ErrorText from "../../../LoginComponent/ErrorText/ErrorText";
import LoginBtn from "../../../LoginComponent/LoginBtn/LoginBtn";
import LoginInput from "../../../LoginComponent/LoginInput/LoginInput";
import LoginPageLayout from "../../../LoginLayout/LoginPageLayout/LoginPageLayout";
import { change_password } from "../../../API/urlconstantsales";
import ToastMessage from "../../../Helper/ToastMessage";


const VerifyOtp = () => {
    const navigate = useNavigate()
    const location = useLocation();
    const [OTP, setOTP] = useState()
    const [loading, setloading] = useState(false)
    const [Error, setError] = useState({
        OTP: false,
        OTPError: ""
    })
    const { t } = useTranslation();
    const onSubmit = async (e) => {
        e.preventDefault()
        try {
            setloading(true)
            !OTP.length >= 1 && setError(prevState => ({ ...prevState, OTP: true, OTPError: t("This feild is required") }))
            if (OTP.length === 4) {
                const result = await BaseUrl.post(verify_otp, {
                    otp: OTP,
                    email: location.state.email,
                    timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                })
                if (result.data.status === true) {
                    setloading(false)
                    const UserId = result.data.email_id
                    const EmailSend = location.state.email
                    navigate(change_password, {
                        state: {
                            UserId,
                            EmailSend
                        }
                    })
                }
                else {
                    ToastMessage("danger", result.data.message)
                }
            } else {
                setloading(false)
            }
        } catch (error) {
            setloading(false)
        } finally {
            setloading(false)
        }
    }
    const [counter, setCounter] = useState(59)
    useEffect(() => {
        setOTP(location.state.NewOtp)
    }, [])
    useEffect(() => {
        const timer = counter > 0 && setInterval(() => {
            setCounter(counter - 1)
        }, 1000);

        return () => {
            clearInterval(timer)
        }

    }, [counter])

    const callBack = (value, type) => {

        if (type === "OTP") {
            const valid = Number(value)
            if (!isNaN(valid)) {
                if (value.length >= 4 && value.length <= 4) {
                    setOTP(value)
                    setError(prevState => ({ ...prevState, OTP: false, OTPError: "" }))

                }
                else if (value.length === 0) {
                    setOTP(value)
                    setError(prevState => ({ ...prevState, OTP: true, OTPError: "This field is required" }))
                }
                else {
                    setOTP(value)
                    setError(prevState => ({ ...prevState, OTP: true, OTPError: "OTP length Should be 4" }))
                }
            }
        }
    }
    const ResendOtp = async () => {
        try {
            setloading(true)
            const result = await BaseUrl.post(send_otp, {
                email: location.state.email
            })
            if (result.data.status === true) {
                const resendOtp = result.data.otp.toString()
                setOTP(resendOtp)
                setCounter(59)
                setloading(false)
            }
            else {
            }

        } catch (error) {
            console.log(error)
        } finally {
            setloading(false)
        }
    }
    // hello
    // hell

    return (
        <LoginPageLayout>
            <form className="RightLoginFeild" onSubmit={onSubmit}>
                <div className="forgot_heading">
                    <p className='heading_top'>Forgot Password</p>
                    <p className='heading_P'>Please input an authorization code sent via SMS to {location.state.email}</p>
                </div>

                <div className='RightLoginItem'>
                    <LoginInput type="text" Label={"Code"} Placeholder={"Enter your Code here"} value={OTP} callBack={callBack} fieldType='OTP' error={Error.OTP} />
                    {Error.OTP ? <ErrorText error={Error.OTPError} /> : null}
                </div>
                <div className='RightLoginItem'>
                    <LoginBtn BtnText={"SUBMIT"} loading={loading} />
                </div>

                <div className="forgot_bottom">
                    {counter !== 0 ? (<p> OTP Resend in : <span className='timer'>{counter} seconds</span> </p>) :
                        (<p onClick={ResendOtp} className="resend_text"><span> RESEND</span> </p>)}
                </div>
                <div>
                </div>

            </form>
            <ToastContainer />
        </LoginPageLayout>
    )
}

export default VerifyOtp;