const mainData = [
    {
        product_name : "Mild",
        amount : "18"
    },
    {
        product_name : "Regular",
        amount : "18"
    },
    {
        product_name : "Advance",
        amount : "18"
    },
    {
        product_name : "Goldflake",
        amount : "18"
    },
    {
        product_name : "American",
        amount : "13"
    },
    {
        product_name : "black",
        amount : "10"
    },
    {
        product_name : "flake",
        amount : "8"
    },
]

export default mainData



















// import { useTranslation } from 'react-i18next';
// import DrawerMenu from '../Drawer/DrawerMenu';
// import { useState } from "react";
// import { useEffect } from "react";
// import { BaseUrl, salesman_id, showAllKPIdata } from "../../API/BaseUrl";
// import Loader from "../../Helper/Loader";
// import SalesLayout from "../../Layout/SalesLayout/SalesLayout";
// import "./SalesKPI.css"
// const SalesKPI = () => {
//     const salesman_id = localStorage.getItem("salesman_id")
//     const [valueData, setValueData] = useState([])
//     const [volumeData, setVolumeData] = useState([])
//     const [outletData, setOutletData] = useState([])
//     const [skuBillData, setSkuBillData] = useState([])
//     const [loading, setLoading] = useState(false)

//     const ApiExtract = async () => {
//         try {
//             setLoading(true)
//             const { data } = await BaseUrl.post(showAllKPIdata, {
//                 salesman_id: salesman_id
//             })
//             setValueData(data.KPI_value_data)
//             setVolumeData(data.KPI_volume_data)
//             setSkuBillData(data.KPI_skus_data)
//             setOutletData(data.KPI_outlet_data)
//         } catch (error) {

//         } finally {
//             setLoading(false)
//         }
//     }

//     useEffect(() => {
//         ApiExtract()
//     }, [])
//     return (
//         <SalesLayout>
//             {loading ? <Loader /> :
//                 <div className='sales_kpi'>
//                     <div className="salesKpiHead">
//                         <div className="kpi_logo_section">
//                             <div className="month_section">
//                                 <h2>SALES KPI</h2> <span className="kpi_date">10 February</span>
//                             </div>
//                             <p>10th February 2022</p>
//                         </div>
//                         <div className="kpi_discription">
//                             <div className="top_kpi_filter">
//                                 <div className="kpi_filter">
//                                     <p>Salesman Name:</p>
//                                     <input type="text" disabled value="Salesman name" />
//                                 </div>
//                                 <div className="kpi_filter">
//                                     <p>Sales Group:</p>
//                                     <input type="text" disabled value="Group" />
//                                 </div>
//                             </div>
//                             <div className="bottom_kpi_filter">
//                                 <div className="kpi_filter">
//                                     <p>Total working days:</p>
//                                     <input type="text" disabled value="Salesman name" />
//                                 </div>
//                                 <div className="kpi_filter">
//                                     <p>Day:</p>
//                                     <input type="text" disabled value="Group" />
//                                 </div>
//                                 <div className="kpi_filter">
//                                     <p>MTD:</p>
//                                     <input type="text" disabled value="Group" />
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                     <div className="CustomerManagement kpi_table">
//                         <table>
//                             <thead>
//                                 <tr>
//                                     <th>KPIs</th>
//                                     <th>Topic</th>
//                                     <th>ITEMS</th>
//                                     <th>TARGET</th>
//                                     <th>Actual</th>
//                                     <th>
//                                         <p> Actual/target</p>
//                                         <p> (%)</p>
//                                     </th>
//                                     <th>MTD</th>
//                                     <th>
//                                         <p> Actual/MTD</p>
//                                         <p> (%)</p>
//                                     </th>
//                                 </tr>
//                             </thead>
//                             <tbody>
//                                 <tr>
//                                     <td><p>Value</p></td>
//                                     <td></td>
//                                     <td>{valueData.map((item) => {
//                                         return (
//                                             <ul>
//                                                 <li>
//                                                     <p>{item.items}</p>
//                                                 </li>
//                                             </ul>
//                                         )
//                                     })}</td>
//                                     <td>{valueData.map((item) => {
//                                         return (
//                                             <ul>
//                                                 <li>
//                                                     <p>{item.amount}</p>
//                                                 </li>
//                                             </ul>
//                                         )
//                                     })}</td>

//                                     <td></td>
//                                     <td></td>
//                                     <td></td>
//                                     <td></td>
//                                 </tr>

//                                 <tr>
//                                     <td><p>Volume</p></td>
//                                     <td></td>
//                                     <td><ul className="volume_list">{volumeData.map((item) => {
//                                         return (
//                                             <li><p> {item.group_product_name} </p></li>
//                                         )
//                                     })}
//                                     </ul></td>
//                                     <td>
//                                         <ul className="volume_list">{volumeData.map((item) => {
//                                             return (
//                                                 <li><p> {item.amount}</p></li>
//                                             )
//                                         })}
//                                         </ul>
//                                     </td>
//                                     <td></td>
//                                     <td></td>
//                                     <td></td>
//                                     <td></td>
//                                 </tr>
//                                 <tr>
//                                     <td><p>Outlet</p></td>
//                                     <td></td>
//                                     <td></td>
//                                     <td></td>
//                                     <td></td>
//                                 </tr>
//                                 <tr>
//                                     <td><p>SKUs</p></td>
//                                     <td></td>
//                                     <td>
//                                         <ul>
//                                             {skuBillData.map((item) => {
//                                                 return(
//                                                     <li><p>{item.sales_code}</p></li>
//                                                 )
//                                             })}
//                                         </ul>
//                                     </td>
//                                     <td>
//                                     <ul>
//                                             {skuBillData.map((item) => {
//                                                 return(
//                                                     <li><p>{item.target}</p></li>
//                                                 )
//                                             })}
//                                         </ul>
//                                     </td>
//                                     <td></td>
//                                     <td></td>
//                                     <td></td>
//                                     <td></td>
//                                 </tr>
//                                 <tr>
//                                     <td><p>Bill</p></td>
//                                     <td></td>
//                                     <td></td>
//                                     <td></td>
//                                     <td></td>
//                                 </tr>
//                             </tbody>

//                         </table>
//                     </div>
//                 </div>}
//         </SalesLayout>
//     )
// }

// export default SalesKPI;