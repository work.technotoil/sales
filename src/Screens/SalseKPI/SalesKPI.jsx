
// import { useTranslation } from 'react-i18next';
// import DrawerMenu from '../Drawer/DrawerMenu';
import { useState } from "react";
import { useEffect } from "react";
import { BaseUrl, showAllKPIdata } from "../../API/BaseUrl";
import Loader from "../../Helper/Loader";
import SalesLayout from "../../Layout/SalesLayout/SalesLayout";
import { checkAvailable } from "../../Utils/helper";
import "./SalesKPI.css"
const SalesKPI = () => {
    const salesman_id = localStorage.getItem("salesman_id")
    const [valueData, setValueData] = useState([])
    const [volumeData, setVolumeData] = useState([])
    const [outletData, setOutletData] = useState([])
    const [skuBillData, setSkuBillData] = useState([])
    const [apiResponse, setApiResponse] = useState({})
    const [effectiveCall, setEffectiveCall] = useState({})
    const [loading, setLoading] = useState(false)

    const ApiExtract = async () => {
        try {
            // debugger
            setLoading(true)
            const { data } = await BaseUrl.post(showAllKPIdata, {
                salesman_id: salesman_id
            })
            setApiResponse(data)
            setValueData(data.KPI_value_data)
            setVolumeData(data.KPI_volume_data)
            setSkuBillData(data.KPI_skus_data)
            setOutletData(data.KPI_outlet_data)
            setEffectiveCall(data.effective_call)
        } catch (error) {

        } finally {
            setLoading(false)
        }
    }
    useEffect(() => {
        ApiExtract()
    }, [])

    const checkIndex = (arr, ind) => {
        if (ind <= 1) {
            return true
        }
    }
    const checkSpan = (arr, ind) => {
        let position
        if (ind === 0) {
            position = 1
        } else if (ind === 1) {
            position = arr.length - 1
        }
        return position
    }
    const tableText = (ind, firstText, secondText) => {

        if (ind === 0) {
            return firstText
        } else {
            return secondText
        }
    }

    const addClassname = (arr, ind) => {
        let tdClass
        if (ind === (arr.length - 1)) {
            tdClass = "last_tr"
        } else if (ind === 0) {
            tdClass = "topTr"
        } else {
            tdClass = "value_tr"
        }
        return tdClass
    }

    return (
        <SalesLayout>
            {loading ? <Loader /> :
                <div className='sales_kpi'>
                    <div className="salesKpiHead">
                        <div className="kpi_logo_section">
                            <div className="month_section">
                                <h2>SALES KPI</h2> <span className="kpi_date">{((apiResponse.current_date)?.slice(4))?.trim() || ""}</span>
                            </div>
                            <p>{apiResponse.current_date}</p>
                        </div>
                        <div className="kpi_discription">
                            <div className="top_kpi_filter">
                                <div className="kpi_filter">
                                    <p>Salesman Name:</p>
                                    <input type="text" disabled value={checkAvailable(apiResponse.salesman_name)} />
                                </div>
                                <div className="kpi_filter">
                                    <p>Sales Group:</p>
                                    <input type="text" disabled value={checkAvailable(apiResponse.sales_group)} />
                                </div>
                            </div>
                            <div className="bottom_kpi_filter">
                                <div className="kpi_filter">
                                    <p>Total working days:</p>
                                    <input type="text" disabled value={checkAvailable(apiResponse.Total_working_days)} />
                                </div>
                                <div className="kpi_filter">
                                    <p>Day:</p>
                                    <input type="text" disabled value={checkAvailable(apiResponse.total_working_days)} />
                                </div>
                                <div className="kpi_filter">
                                    <p>MTD:</p>
                                    <input type="text" disabled value={checkAvailable(apiResponse.total_working_days)} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="kpi_table CustomerManagement  ">
                        <table>
                            <thead>
                                <tr>
                                    <th>KPIs</th>
                                    <th>Topic</th>
                                    <th>ITEMS</th>
                                    <th>TARGET</th>
                                    <th>Actual</th>
                                    <th>
                                        <p> Actual/target</p>
                                        <p> (%)</p>
                                    </th>
                                    <th>MTD</th>
                                    <th>
                                        <p> Actual/MTD</p>
                                        <p> (%)</p>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>  <td className="kpi_name" rowSpan={valueData?.length + 1}><p>Value</p></td> </tr>
                                { valueData?.map((item, index) => {
                                    const { group_product_name, amount, actual, percent } = item
                                    return (
                                        <tr className={addClassname(valueData, index)}>
                                            {checkIndex(valueData, index) ? <td rowSpan={checkSpan(valueData, index)} className={tableText(index, "Value", "Special Target") === "Special Target" ? "valueTd bottomTr" : "valueTd"}>{tableText(index, "Value", "Special Target")}</td> : ""}
                                            <td className="valueTd"><p> {group_product_name}</p></td>
                                            <td className="valueTd">{amount}</td>
                                            <td className="valueTd">{actual?.toFixed(2)}</td>
                                            <td className="valueTd">{percent?.toFixed(2)}</td>
                                            <td className="valueTd">{checkAvailable(item.mtd)}</td>
                                            <td className="valueTd">{checkAvailable(item.actual_mtd)}</td>
                                        </tr>
                                    )
                                })}
                                <tr>  <td className="kpi_name" rowSpan={volumeData?.length + 1}><p>Volume</p></td> </tr>
                                {volumeData?.map((item, index) => {
                                    const { group_product_name, amount, actual, percent } = item
                                    return (
                                        <tr className={index === (volumeData.length - 1) ? "last_tr" : "value_tr"}>
                                            {/* <td className="valueTd">Volume</td> */}
                                            {index === 0 ? <td rowSpan={volumeData.length} className="volume_tr valueTd">{tableText(index, "Volume", "Special Target")}</td> : ""}
                                            <td className="valueTd">{group_product_name}</td>
                                            <td className="valueTd">{amount}</td>
                                            <td className="valueTd">{actual}</td>
                                            <td className="valueTd">{percent?.toFixed(2)}</td>
                                            <td className="valueTd">{checkAvailable(item.mtd)}</td>
                                            <td className="valueTd">{checkAvailable(item.actual_mtd)}</td>
                                        </tr>
                                    )
                                })}
                                <tr><td className="kpi_name" rowSpan={outletData?.length + 1}><p>Outlet</p></td> </tr>
                                {outletData?.map((item, index) => {
                                    const { group_product_name, amount, actual, percent } = item
                                    return (
                                        <tr className={addClassname(outletData, index)}>
                                            {/* <td className="valueTd"></td> */}
                                            {checkIndex(outletData, index) ? <td rowSpan={checkSpan(outletData, index)} className={tableText(index, "Effective Outlet", "New Outlet by Product") === "New Outlet by Product" ? "valueTd bottomTr" : "valueTd"}>{tableText(index, "Effective Outlet", "New Outlet by Product")}</td> : ""}
                                            <td className="valueTd">{group_product_name}</td>
                                            <td className="valueTd">{amount}</td>
                                            <td className="valueTd">{actual}</td>
                                            <td className="valueTd">{percent?.toFixed(2)}</td>
                                            <td className="valueTd">{checkAvailable(item.mtd)}</td>
                                            <td className="valueTd">{checkAvailable(item.actual_mtd)}</td>
                                        </tr>
                                    )
                                })}
                                <tr><td className="kpi_name" rowSpan={skuBillData?.length + 1}><p>Sku Bill data</p></td> </tr>
                                {skuBillData?.map((item, index) => {
                                    const { group_product_name, target, actual, percent } = item
                                    return (
                                        <tr className={addClassname(skuBillData, index)}>
                                            {checkIndex(skuBillData, index) ? <td rowSpan={checkSpan(skuBillData, index)} className={tableText(index, "SKU/Bill", "Product Group/Outlet") === "Product Group/Outlet" ? "valueTd bottomTr" : "valueTd"}>{tableText(index, "SKU/Bill", "Product Group/Outlet")}</td> : ""}
                                            <td className="valueTd">{group_product_name}</td>
                                            <td className="valueTd">{target}</td>
                                            <td className="valueTd">{actual}</td>
                                            <td className="valueTd">{percent?.toFixed(2)}</td>
                                            <td className="valueTd">{checkAvailable(item.mtd)}</td>
                                            <td className="valueTd">{checkAvailable(item.actual_mtd)}</td>
                                        </tr>
                                    )
                                })}
                                <tr><td className="kpi_name" rowSpan={skuBillData?.length + 1}><p>Effective Call</p></td> </tr>
                                <tr className="last_tr">
                                    <td className="valueTd bottomTr">Effective call</td>
                                    <td></td>
                                    <td className="valueTd">{effectiveCall?.effective_call ? effectiveCall?.effective_call.toFixed(2) : effectiveCall?.effective_call}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>

                </div>}
        </SalesLayout >
    )
}

export default SalesKPI;