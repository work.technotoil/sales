/* eslint-disable linebreak-style */
import React, { useState } from "react"
import { useTranslation } from "react-i18next"
import { useNavigate } from "react-router-dom"
import "./Index.css"
import { BaseUrl, change_password } from "../../API/BaseUrl"
import SalesLayout from "../../Layout/SalesLayout/SalesLayout.jsx"
import ErrorText from "../../LoginComponent/ErrorText/ErrorText.jsx"
import LoginBtn from "../../LoginComponent/LoginBtn/LoginBtn.jsx"
import LoginInput from "../../LoginComponent/LoginInput/LoginInput.jsx"
import OpenEye from "../../Helper/OpenEye"
import CloseEye from "../../Helper/CloseEye"
import ToastMessage from "../../Helper/ToastMessage"

const CustomerChangePass = ({ protect }) => {

  const navigate = useNavigate()
  const [password1, setPassword1] = useState("")
  const [password2, setPassword2] = useState("")
  const [loading, setloading] = useState(false)
  const [oldPassword, setOldPassword] = useState("")
  const [eyeImage, setEyeImage] = useState(false)
  const [showPassword, setShowPassword] = useState(false)
  const [eyeImage2, setEyeImage2] = useState(false)
  const [oldEyeImage, setOldEyeImage] = useState(false)
  const [showPassword2, setShowPassword2] = useState(false)
  const [showOldPassword, setShowOldPassword] = useState(false)

  const [Error, setError] = useState({
    password1: false,
    password1Error: "",
    password2: false,
    password2Error: "",
    oldPassword: false,
    oldPasswordError: ""
  })
  const { t } = useTranslation()
  const onSubmit = async (e) => {
    e.preventDefault()
    try {
      setloading(true)
      password2 !== password1 && setError(prevState => ({ ...prevState, password2: true, password2Error: t("Password and confirm password should be same") }))
      !password1.length >= 1 && setError(prevState => ({ ...prevState, password1: true, password1Error: t("This feild is required") }))
      !password2.length >= 1 && setError(prevState => ({ ...prevState, password2: true, password2Error: t("This feild is required") }))
      !oldPassword.length >= 1 && setError(prevState => ({ ...prevState, oldPassword: true, oldPasswordError: t("This feild is required") }))
      if (!Error.password1 && !Error.password2 && !Error.oldPassword && password1.length >= 1 && password2.length >= 1 && oldPassword.length >= 1 && password1 === password2) {   //checking all required fields
        const result = await BaseUrl.post(change_password, {
          old_password: oldPassword,
          new_password: password1,
          email_id: localStorage.token
        })
        if (result.data.status === true) {
          setloading(false)
          localStorage.clear();
          navigate("/")
        }
        else {
          setloading(false)
          ToastMessage("danger", result.data.message)
        }

      }
      else {
        setloading(false)
      }
    } catch (error) {
      setloading(false)
      ToastMessage("danger", error.message)
    }
  }

  const callBack = (value, type) => {
    if (type === "password1") {
      setPassword1(value)
      if (value.length >= 6) {
        if(password2.length>5){
          if(value=== password2){
            setError(prevState => ({ ...prevState, password2: false, password2Error: "" }))
          }else{
            setError(prevState => ({ ...prevState, password2: true, password2Error: "Password and confirm password should be same" }))
          }
        }
        setError(prevState => ({ ...prevState, password1: false, password1Error: "" }))
      }
      else if (value.length === 0) {
        setPassword1(value)
        setError(prevState => ({ ...prevState, password1: true, password1Error: t("This feild is required") }))
      }
      else {
        setPassword1(value)
        setError(prevState => ({ ...prevState, password1: true, password1Error: t("Password should be atleast 6 digit") }))
      }
    }
    else if (type === "oldPassword") {
      setOldPassword(value)
      if (value.length >= 6) {
        setError(prevState => ({ ...prevState, oldPassword: false, oldPasswordError: "" }))
      }
      else if (value.length === 0) {
        setOldPassword(value)
        setError(prevState => ({ ...prevState, oldPassword: true, oldPasswordError: t("This feild is required") }))
      }
      else {
        setOldPassword(value)
        setError(prevState => ({ ...prevState, oldPassword: true, oldPasswordError: t("Password should be atleast 6 digit") }))
      }
    }
    else {
      if (value === password1) {
        setPassword2(value)
        setError(prevState => ({ ...prevState, password2: false, password2Error: "" }))
      }
      else if (value.length === 0) {
        setPassword2(value)
        setError(prevState => ({ ...prevState, password2: true, password2Error: t("This feild is required") }))
      }
      else {
        setPassword2(value)
        setError(prevState => ({ ...prevState, password2: true, password2Error: t("Password and confirm password should be same") }))
      }
    }
  }
  const ChangeImage = () => {
    setEyeImage(!eyeImage)
  }

  const PasswordShow = () => {
    setShowPassword(!showPassword)
  }
  const ChangeImage2 = () => {
    setEyeImage2(!eyeImage2)
  }
  const PasswordShow2 = () => {
    setShowPassword2(!showPassword2)
  }
  const ChangeOldImage = () => {
    setOldEyeImage(!oldEyeImage)
  }
  const oldPasswordShow = () => {
    setShowOldPassword(!showOldPassword)
  }

  return (
    <SalesLayout>
      {/* <UserProfileLayout> */}
      <form className="changePassFeild" onSubmit={onSubmit}>
        <div className="Heading">
          CHANGE PASSWORD
        </div>
        <div className="RightLoginItem">
          <LoginInput Label="Current Password" Placeholder="Enter your password here" value={oldPassword} type={showOldPassword ? "text" : "password"} fieldType='oldPassword' callBack={callBack} error={Error.oldPassword} />
          <span onClick={ChangeOldImage} >{oldEyeImage ? < OpenEye onClick={oldPasswordShow} /> : < CloseEye onClick={oldPasswordShow} />}</span>
          {Error.oldPassword ? <ErrorText error={Error.oldPasswordError} /> : null}
        </div>
        <div className="RightLoginItem">
          <LoginInput Label="New Password" Placeholder="Enter your password here" value={password1} type={showPassword ? "text" : "password"} fieldType='password1' callBack={callBack} error={Error.password1} />
          <span onClick={ChangeImage} >{eyeImage ? < OpenEye onClick={PasswordShow} /> : < CloseEye onClick={PasswordShow} />}</span>
          {Error.password1 ? <ErrorText error={Error.password1Error} /> : null}
        </div>
        <div className="RightLoginItem">
          <LoginInput Label="Confirm New Password" Placeholder="Enter your password here" value={password2} type={showPassword2 ? "text" : "password"} fieldType='password2' callBack={callBack} error={Error.password2} />
          <span onClick={ChangeImage2} >{eyeImage2 ? < OpenEye onClick={PasswordShow2} /> : < CloseEye onClick={PasswordShow2} />}</span>
          {Error.password2 ? <ErrorText error={Error.password2Error} /> : null}
        </div>
        <div className='RightLoginItem'>

          <LoginBtn BtnText="SAVE CHANGES" loading={loading} />

        </div>
      </form>

    </SalesLayout>
  )
}

export default CustomerChangePass;

