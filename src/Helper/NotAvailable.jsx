import React from 'react'

const NotAvailable = () => {
  return (
    <div style={{textAlign:"center"}}>
      N/A
    </div>
  )
}

export default NotAvailable
