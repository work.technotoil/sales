import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';


const DropDown = ({ defaultDrop , defaultVal , DropText, RouteNameList ,type , onSelect, disabled ,Screen,filterClass, onClick}) => {
  const MainArray = RouteNameList || [];
  let options = []
  MainArray.map((item) => {
    let newObj = {}
    newObj.value = item.id
    newObj.label = item.route_name || item.area_name || item.route_no
    options.push(newObj)
  })

  const SelectedOption = {
    value : "",
    label : "Please select...",
  
  }
  
  options.unshift(SelectedOption)
  
  const MainOption = options.filter((data) => {
    if(data.label){
      return data
    }
  })
  return (
    <Dropdown value={defaultVal} className={disabled ? "DisableDrop" : "EnableDrop"} onFocus={(data)=>onClick(data , type)} options={MainOption.length ? MainOption : ""} disabled={disabled} onChange={(data)=>onSelect(data , type)}  placeholder={DropText} />

  )
}
export default DropDown




















