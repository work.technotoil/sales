import React from 'react';
import { message, Popconfirm } from 'antd';

const ConfirmAlert = ({AlertBtnText , Onconfirm , Oncancel}) => {
    return (
        <Popconfirm
            title="Are you sure to logout?"
            onConfirm={Onconfirm}
            placement="topLeft"
            onCancel={Oncancel}
            okText="Yes"
            cancelText="No"
        >
            <p className='popConfirmPara'>{AlertBtnText}</p>
        </Popconfirm>

    )
}
export default ConfirmAlert;