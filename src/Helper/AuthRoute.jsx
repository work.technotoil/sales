import { useEffect, useState } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import asyncLocalStorage from "./asyncLocalStorage"
import { right_login } from "../API/urlconstantsales";


const AuthRoute = () => {
    const [isAuth, setisAuth] = useState(false);
    const navigate = useNavigate();
    useEffect(() => {
        (async () => {
            try {
                const token = await asyncLocalStorage.getItem("token");

                if (token) {
                    setisAuth(true);
                } else {
                    setisAuth(false);
                    navigate(right_login);
                }
            } catch (error) {
                console.log(error)
            }
        })();
    }, []);

    return isAuth ? <Outlet /> : ""
};

export default AuthRoute;


