import React from 'react';
import { Tooltip } from 'antd';
import "./Style.css"
const ToolTip = ({TitleText , ParaText}) =>{
return(
  <Tooltip color='white' placement="topLeft" title={TitleText}>
    <span>{ParaText}</span>
  </Tooltip>
)}
export default ToolTip;
