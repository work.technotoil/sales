import Nodata from "../image/NoData.jpg"
import Cross from "../image/cross.svg"
import Checked from "../image/checkedbox.svg"
import unChecked from "../image/uncheckedbox.svg"
import excelEport from "../image/download_excel.svg"
import shopfastlogo from "../image/shopfastlogo.png"

export const Nodatas = Nodata
export const Crosss = Cross
export const checkedBox = Checked
export const uncheckedbox = unChecked
export const excelEportIcon = excelEport
export const Shopfastlogo = shopfastlogo
