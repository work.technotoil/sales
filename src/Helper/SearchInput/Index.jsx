import React from 'react'
import "./Index.css"
import Searchicon from "../../image/search icon.svg"
import Clearbtn from "../../image/clearbutton.png"

const SearchInput = ({value, placeholder, SearchText, type ,func}) => {

  return (
    <div className='SearchInput'>
        
      <input value={value} type="text" placeholder={placeholder} onChange={(e) => { SearchText(e.target.value, type, e) }} />
      
       { value.length >=1  ?  <img className='clearbtnimg' onClick={func} src={Clearbtn} alt="" />  :  <img src={Searchicon} alt="" /> } 
      
    </div>
  )
}

export default SearchInput















