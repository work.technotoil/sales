import React from "react"
import EyeClose from "../image/closeEye.png"
const CloseEye = ({onClick}) => {
  return (
    <div className='EyeDiv'>
      <img src={EyeClose} alt="" style={{width:"18px" , cursor:"pointer"}} onClick={onClick}/>
    </div>
  )
}

export default CloseEye