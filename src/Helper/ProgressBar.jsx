import { Progress } from 'antd';
import React, { useEffect, useState } from 'react';
import 'antd/dist/antd.css';
import "./Progress.css"

const ProgressBar = ({ Count }) => {
  const [BarLength, setBarLength] = useState(0)
  useEffect(() => {
    if (Count >= 5) {
      setBarLength(Count - 1)
    } else {
      setBarLength(Count)
    }
  }, [])
  return (
    <>
      <Progress percent={(100 / 5) * BarLength} showInfo={false} />
    </>
  )
};

export default ProgressBar;