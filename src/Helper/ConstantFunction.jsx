import { useEffect } from "react"
import { BaseUrl, get_route_name } from "../API/BaseUrl"


export const RouteNameApi = async () => {
    try {
        const Response = await BaseUrl.post(get_route_name)
        return Response.data
    } catch (error) {

    } 
}


