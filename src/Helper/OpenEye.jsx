import React from "react"
import EyeOpen from "../image/openEye.png"
const OpenEye = ({onClick}) => {
  return (
    <div className='EyeDiv'>
      <img src={EyeOpen} alt="" style={{width:"18px" , cursor:"pointer"}} onClick={onClick}/>
    </div>
  )
}

export default OpenEye