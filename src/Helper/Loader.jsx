import React from 'react'
import { SpinnerDotted } from 'spinners-react'

const Loader = () => {
  return (
    <div className='LoadingBg'> <SpinnerDotted size={100} thickness={100} speed={100} color="rgba(0, 217, 255, 1)" /> </div>
  )
}

export default Loader