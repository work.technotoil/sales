import React from 'react'
import { Nodatas } from './ImagePath'
import "./Style.css"

const Nodata = () => {
  return (
    <div className='Nodata'>
        <img src={Nodatas} alt="" />
    </div>
  )
}

export default Nodata