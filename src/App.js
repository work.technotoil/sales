import { Fragment, useEffect, useState } from "react";
import {
  Navigate,
  Route,
  BrowserRouter as Router,
  Routes,
} from "react-router-dom";
import "./App.css";
import LoginPageLayout from "./LoginLayout/LoginPageLayout/LoginPageLayout";
import RightLogin from "./Screens/LoginScreen/RightLogin/RightLogin";
import OdrMonitor from "./Screens/OdrMonitor/OdrMonitor";
import OrderMonitor from "./Screens/OrderMonitor/OrderMonitor";
import SalesKPI from "./Screens/SalseKPI/SalesKPI";
import ForgotPassword from "./Screens/LoginScreen/ForgotPassword/ForgotPassword";
import VerifyOtp from "./Screens/LoginScreen/VerifyOtp/VerifyOtp";
import ChangePassword from "./Screens/LoginScreen/ChangePassword/ChangePassword";
import DashBoard from "./Screens/CustomerManagement/DashBoard";
import SaleHistory from "./Screens/SaleHistory/Index";
import CustomerChangePass from "./Screens/CustomerChangePass/CustomerChangePass";
import AuthRoute from "./Helper/AuthRoute";
import DailyCallCard from "./Screens/DailyCallCard/Index";
import {
  dashboard,
  order_monitor_sale_history,
  right_login,
  sale_history,
} from "./API/urlconstantsales";
import Pusher from "pusher-js";
import ToastMessage from "./Helper/ToastMessage";
function App() {
  const [loadingApp, setLoadingApp] = useState(false);
  const token = localStorage.getItem("token");
  const path = token ? dashboard : right_login;
  useEffect(() => {
    var pusher = new Pusher("d7d058d7ebfda821aed0", {
      cluster: "ap2",
    });
    var channel = pusher.subscribe("my-channel");
    var saleaman_id = localStorage.getItem("salesman_id");
    channel.bind(`salesman_${saleaman_id}`, function (data) {
      localStorage.clear();
      ToastMessage("danger", data.message);
      window.location.reload();
    });
  }, [loadingApp]);
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path="/" element={<Navigate to={path} replace />} />
          <Route
            path={right_login}
            element={<RightLogin setLoadingApp={setLoadingApp} />}
          />
          <Route path="/ForgotPassword" element={<ForgotPassword />} />
          <Route path="/VerifyOtp" element={<VerifyOtp />} />
          <Route path="/ChangePassword" element={<ChangePassword />} />
          <Route path="LoginPageLayout" element={<LoginPageLayout />} />
          <Route element={<AuthRoute />}>
            <Route path={dashboard} element={<DashBoard />} />
            <Route path={sale_history} element={<SaleHistory />} />
            <Route
              path={order_monitor_sale_history}
              element={<SaleHistory />}
            />
            <Route path="/OrderMonitor" element={<OdrMonitor />} />
            <Route path="/OrderReport" element={<OrderMonitor />} />
            <Route path="/SalesKPI" element={<SalesKPI />} />
            <Route
              path="/CustomerChangePass"
              element={<CustomerChangePass />}
            />
            <Route path="/DailyCallCard" element={<DailyCallCard />} />
          </Route>
        </Routes>
      </Router>
    </Fragment>
  );
}

export default App;
