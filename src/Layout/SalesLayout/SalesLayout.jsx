import React from 'react'
import ContentBar from '../ContentBar/ContentBar'
import SideNavbar from '../SideNavbar/SideNavbar'
import "./SalesLayout.css"

const SalesLayout = ({children}) => {
    return (
        <div className='SalesLayout'>
            <div className="LeftPanel">
                <SideNavbar />
            </div>
            <div className="RightPanel">
                <ContentBar child={children}/>
            </div>
        </div>
    )
}

export default SalesLayout