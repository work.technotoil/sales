import React from 'react'
import { useNavigate } from "react-router-dom"
import Logo from '../../image/logo.svg'
import Customer from "../../image/Users.svg"
import Routing from "../../image/Routing.svg"
import SalesKpi from "../../image/Star.svg"
import Orders from "../../image/Orders.svg"
import Reports from "../../image/Reports.svg"
import Profile from "../../image/Rectangle 115.svg"
import Logout from "../../image/icons8-logout-96.png"
import "./SideNavabr.css"
import { NavLink } from "react-router-dom"
import ChangePass from "../../image/ChangePass.svg"
import ConfirmAlert from '../../Helper/ConfirmAlert/Index'
import { right_login ,dashboard ,order_monitor ,order_report ,sales_kpi ,customer_change_pass ,daily_call_card} from '../../API/urlconstantsales'
import { Shopfastlogo } from '../../Helper/ImagePath'

const SideNavbar = () => {
    const navigate = useNavigate()

    const Onconfirm = () => {
        localStorage.clear()
        navigate(right_login)
    }
    // dharmesh
    const logoClick =()=>{
        navigate(dashboard)
    }
    // dharmesh
    const salesman_name = localStorage.getItem("salesman_name")
    return (
        <div className="sidenav">
            <div className="topSidenav">
            {/* dharmesh */}
                <div onClick={logoClick} className="logoSection">
                    <div className="logoImg">
                        <img src={Shopfastlogo} alt="" />
                    </div>
                    <p className="logoName">
                        SHOPFAST
                    </p>
                </div>
                {/* dharmesh */}
                <div className="salesRoutes">
                    <NavLink to={dashboard} className={(navData) => (navData.isActive ? "activeLink" : "NotActivelink")}><img src={Customer} alt="" /><p className='hoverPara'>CUSTOMERS</p></NavLink>
                    {/* <NavLink to="SalesHistory" style={{pointerEvents:"none"}} aria-disabled className={(navData) => (navData.isActive ? "activeLink" : "NotActivelink")}><img src={Routing} alt="" /><p className='hoverPara'>ROUTING</p></NavLink> */}
                    <NavLink to={daily_call_card} className={(navData) => (navData.isActive ? "activeLink" : "NotActivelink")}><img src={Routing} alt="" /><p className='hoverPara routingpara'>ROUTING & CALL CARD</p></NavLink>
                    <NavLink to={sales_kpi} className={(navData) => (navData.isActive ? "activeLink salesImg" : "NotActivelink salesImg")}> <img src={SalesKpi} alt="" /><p className='hoverPara'>SALES KPI</p></NavLink>
                    <NavLink to={order_monitor} className={(navData) => (navData.isActive ? "activeLink" : "NotActivelink")}><img src={Orders} alt="" /><p className='hoverPara'>ORDERS</p></NavLink>
                    <NavLink to={order_report} className={(navData) => (navData.isActive ? "activeLink" : "NotActivelink")}><img src={Reports} alt="" /><p className='hoverPara'>REPORTS</p></NavLink>              
                </div>
            </div>
            <div className="BottomSidenav">
                <ul className="logChange">
                    <li className="changePass salesRoutes" ><NavLink to={customer_change_pass} className={(navData) => (navData.isActive ? "activeLink " : "NotActivelink")}><span><img className='LogoutImg' onClick={() => navigate(customer_change_pass)} src={ChangePass} alt="" /></span><p className='hoverPara'> <u onClick={() => navigate(customer_change_pass)}>Change password</u> </p></NavLink> </li>
                    {/* <li onClick={Logouts} className="logout" ><span ><img className='LogoutImg' src={Logout} alt="" /></span> <u className='HoverText' onClick={Logouts}>Log Out</u> </li> */}
                    <li className="logout" >
                        <ConfirmAlert Onconfirm={Onconfirm} AlertBtnText={<> <span ><img className='LogoutImg' src={Logout} alt="" /></span> <u className='HoverText'>Log Out</u></>} />
                    </li>
                </ul>
                <div className="customerProfile">
                    <span><img src={Profile} alt="" /></span> <span><p className='ProfileName'>{salesman_name}</p></span>
                </div>
            </div>
        </div>
    )
}

export default SideNavbar