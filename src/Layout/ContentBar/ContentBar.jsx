import React from 'react'
import DataTableBase from '../../Components/DataTable'
import "./ContentBar.css"

const ContentBar = ({child}) => {
  return (
    <div className='ContentBar'>
        {child}
    </div>
  )
}

export default ContentBar