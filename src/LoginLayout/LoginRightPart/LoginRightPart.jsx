
import React from 'react'

import './LoginRightPart.css'



const LoginRightPart = ({ child }) => {

    return (
        <div className='RightLogin'>
            <div className='RightLoginComp'>
                {child}
            </div>

        </div>
    )
}

export default LoginRightPart