import React from 'react'
import LogoShop from '../../image/acLogo.svg'
import './LoginLeftPart.css'
const LoginLeftPart = () => {
  return (
    <div className='LeftSideBar'>
        <img src={LogoShop} alt="" />
    </div>
  )
}

export default LoginLeftPart