import React from 'react'
import './LoginPageLayout.css'

import LoginLeftPart from '../LoginLeftPart/LoginLeftPart'
import LoginRightPart from '../LoginRightPart/LoginRightPart'


const LoginPageLayout = ({ children }) => {

    return (
        <div className="mainBody">
            <div className='ShopFastMain'>
                <div className='ShopFastLeft'>
                    <LoginLeftPart />
                </div>
                <div className='ShopFastRight'>
                    <LoginRightPart child={children} />
                </div>
            </div>
        </div>
    )
}

export default LoginPageLayout