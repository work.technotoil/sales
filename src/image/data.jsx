import user from '../image/Users.svg';
import chart from '../image/Chart.svg';
import activity from '../image/Activity.svg';
import category from '../image/Category.svg';
import document from '../image/Document.svg';
import Biscuit from '../image/Biscuit.svg';

import users from '../image/Users.png'

export const menuImg = [users, chart, activity, category, document]

export const RedBiscuit = [
    {
        id:1,
        name:"Bento 24g",
        img:Biscuit,
        oct:126,
        nov:160,
        dec:179,
    },
    {
        id:2,
        name:"Bento 24g",
        img:Biscuit,
        oct:126,
        nov:160,
        dec:179,
    },
    {
        id:3,
        name:"Bento 24g",
        img:Biscuit,
        oct:126,
        nov:160,
        dec:179,
    },
    {
        id:4,
        name:"Bento 24g",
        img:Biscuit,
        oct:126,
        nov:160,
        dec:179,
    },
];