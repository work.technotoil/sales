import axios from "axios";

const protectedKey = "shopfast_salesman.wQd=X=v7QgBon6pHFTnVWa?3e84?=ZWFDr1g/vpEnP7ahN2EPaWX0iMQaszO?XbyTRPOlkvpZ7BHLWzpn-BMS/jCJpgQ!tlpF70rrTC/D=in85yD-eVL8XPZ2JpOf2ocpSqfxppdCsgglJiQjPW2kysxDjG/4h/5a9WpNtS7DUpdptKbc5K!ATRgjZQvugNDI8t5hxfsSp9A=2O0xMgvfgpRm0yVJ1adcMIeiCpb2I6Mt4"

// live url
// const ApiUrl = "https://cool-kapitsa.206-189-155-247.plesk.page/api/"
// export const BaseUrlImg = "https://cool-kapitsa.206-189-155-247.plesk.page"
// developement url
const ApiUrl = "https://shopfastnew.technotoil.com/api/"
export const BaseUrlImg = "https://shopfastnew.technotoil.com"


export const BaseUrl = axios.create({
  baseURL: ApiUrl, headers:
  {
    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
    platform: "web",
    key: protectedKey
  }
})

// api url path

export const send_otp = "send_otp_forget"
export const verify_otp = "verify_otp_forget"
export const forget_password = "forget_password"
export const change_password = "change_password"
export const login_sales = "login_sales"
export const all_orders = "all_orders"
export const customer_list = "customer_list"
export const all_customers_list = "all_customers_list"
export const customer_detail = "customer_detail"
export const sales_history = "sales_history"
export const get_route_name = "get_route_name"
export const get_area = "get_area"
export const get_route_no = "get_route_no"
export const category_group_product = "category_group_product"
export const showAllKPIdata = "showAllKPIdata"
export const customer_detail_order_detail = "customer_detail_order_detail"  // Daily call card
// export const Trial = axios.create({
//   baseURL: "https://jsonplaceholder.typicode.com/",
// })
export const Posts = "posts"


